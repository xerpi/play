#pragma once

#include <cinttypes>
#include <psp2/gxm.h>
#include "math_utils.h"

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

struct color_vertex {
	vector3f position;
	uint32_t color;
};

struct texture_vertex {
	vector3f position;
	uint32_t color;
	vector2f texcoord;
};

struct gxm_texture {
	SceGxmTexture texture;
	SceUID data_uid;
	uint32_t stride;
};

struct gxm_palette {
	SceUID data_uid;
	uint32_t *data;
};

int gxm_init();
int gxm_finish();
int gxm_terminate();
void gxm_begin_scene();
void gxm_end_scene();
void gxm_queue_flip();

void gxm_color_surfaces_set_clip(unsigned int x_min, unsigned int y_min, unsigned int x_max, unsigned int y_max);

void gxm_set_color_fragment_program(const SceGxmFragmentProgram *fragment_program);
void gxm_set_texture_fragment_program(const SceGxmFragmentProgram *fragment_program);

void gxm_create_color_fragment_program(const SceGxmBlendInfo *blend_info, SceGxmFragmentProgram **fragment_program);
void gxm_create_texture_fragment_program(const SceGxmBlendInfo *blend_info, SceGxmFragmentProgram **fragment_program);
void gxm_release_fragment_program(SceGxmFragmentProgram *fragment_program);

void gxm_create_texture(struct gxm_texture *texture, uint32_t width, uint32_t height, SceGxmTextureFormat format);
void gxm_texture_set_palette(struct gxm_texture *texture, const struct gxm_palette *palette);
void gxm_texture_set_fragment_texture(const struct gxm_texture *texture, uint32_t texture_index);
void *gxm_texture_get_data(const struct gxm_texture *texture);
uint32_t gxm_texture_get_width(const struct gxm_texture *texture);
uint32_t gxm_texture_get_height(const struct gxm_texture *texture);
uint32_t gxm_texture_get_stride(const struct gxm_texture *texture);
void gxm_texture_set_min_filter(struct gxm_texture *texture, SceGxmTextureFilter filter);
void gxm_texture_set_mag_filter(struct gxm_texture *texture, SceGxmTextureFilter filter);
void gxm_texture_set_mip_filter(struct gxm_texture *texture, SceGxmTextureMipFilter filter);
void gxm_texture_set_u_addr_mode(struct gxm_texture *texture, SceGxmTextureAddrMode mode);
void gxm_texture_set_v_addr_mode(struct gxm_texture *texture, SceGxmTextureAddrMode mode);
void gxm_free_texture(struct gxm_texture *texture);

void gxm_create_palette(struct gxm_palette *palette, SceGxmTextureBaseFormat base_format);
uint32_t *gxm_palette_get_data(struct gxm_palette *palette);
void gxm_free_palette(struct gxm_palette *palette);

void gxm_vertex_buffer_color_push(SceGxmPrimitiveType prim, const struct color_vertex *vertices, uint32_t count);
void gxm_vertex_buffer_texture_push(SceGxmPrimitiveType prim, const struct texture_vertex *vertices, uint32_t count);
void gxm_vertex_buffer_color_reset();
void gxm_vertex_buffer_texture_reset();

void gxm_set_polygon_mode(SceGxmPolygonMode mode);
void gxm_set_depth_write_mode(SceGxmDepthWriteMode mode);
void gxm_set_depth_test_function(SceGxmDepthFunc func);
