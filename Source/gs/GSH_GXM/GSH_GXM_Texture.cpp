#include <assert.h>
#include <cstring>
#include "GSH_GXM.h"
#include "../GsPixelFormats.h"

void CGSH_GXM::SetupTextureUpdaters()
{
	for (unsigned int i = 0; i < PSM_MAX; i++) {
		m_textureUpdater[i] = &CGSH_GXM::TexUpdater_Invalid;
	}

	m_textureUpdater[PSMCT32] = &CGSH_GXM::TexUpdater_Psm32;
	m_textureUpdater[PSMCT24] = &CGSH_GXM::TexUpdater_Psm32;
	m_textureUpdater[PSMCT16] = &CGSH_GXM::TexUpdater_Psm16<CGsPixelFormats::CPixelIndexorPSMCT16>;
	m_textureUpdater[PSMCT16S] = &CGSH_GXM::TexUpdater_Psm16<CGsPixelFormats::CPixelIndexorPSMCT16S>;
	m_textureUpdater[PSMT8] = &CGSH_GXM::TexUpdater_Psm48<CGsPixelFormats::CPixelIndexorPSMT8>;
	m_textureUpdater[PSMT4] = &CGSH_GXM::TexUpdater_Psm48<CGsPixelFormats::CPixelIndexorPSMT4>;
	m_textureUpdater[PSMT8H] = &CGSH_GXM::TexUpdater_Psm48H<24, 0xFF>;
	m_textureUpdater[PSMT4HL] = &CGSH_GXM::TexUpdater_Psm48H<24, 0x0F>;
	m_textureUpdater[PSMT4HH] = &CGSH_GXM::TexUpdater_Psm48H<28, 0x0F>;
}

struct gxm_texture *CGSH_GXM::LoadTexture(const TEX0& tex0, uint32 maxMip, const MIPTBP1& miptbp1, const MIPTBP2& miptbp2)
{
	auto texture = m_textureCache.Search(tex0);
	if (!texture) {
		uint32_t width = tex0.GetWidth();
		uint32_t height = tex0.GetHeight();

		SceGxmTextureFormat format = ToGxmTextureFormat(tex0.nPsm);
		auto gxm_tex = GxmTextureResource::Create(width, height, format);
		m_textureCache.Insert(tex0, std::move(gxm_tex));

		texture = m_textureCache.Search(tex0);
		texture->m_cachedArea.Invalidate(0, RAMSIZE);
	}

	auto texturePageSize = CGsPixelFormats::GetPsmPageSize(tex0.nPsm);
	auto &cachedArea = texture->m_cachedArea;

	while (cachedArea.HasDirtyPages()) {
		auto dirtyRect = cachedArea.GetDirtyPageRect();
		assert((dirtyRect.width != 0) && (dirtyRect.height != 0));
		cachedArea.ClearDirtyPages(dirtyRect);

		uint32 texX = dirtyRect.x * texturePageSize.first;
		uint32 texY = dirtyRect.y * texturePageSize.second;
		uint32 texWidth = dirtyRect.width * texturePageSize.first;
		uint32 texHeight = dirtyRect.height * texturePageSize.second;

		if ((texX >= tex0.GetWidth()) || (texY >= tex0.GetHeight()))
			continue;

		if ((texX + texWidth) > tex0.GetWidth())
			texWidth = tex0.GetWidth() - texX;

		if((texY + texHeight) > tex0.GetHeight())
			texHeight = tex0.GetHeight() - texY;

		((this)->*(m_textureUpdater[tex0.nPsm]))(texture->m_textureHandle, tex0.GetBufPtr(), tex0.nBufWidth, texX, texY, texWidth, texHeight);
	}

	cachedArea.ClearDirtyPages();

	return texture->m_textureHandle.getHandle();
}

void CGSH_GXM::TexUpdater_Invalid(GxmTextureResource &, uint32, uint32, unsigned int, unsigned int, unsigned int, unsigned int)
{
	assert(false);
}

void CGSH_GXM::TexUpdater_Psm32(GxmTextureResource &texture, uint32 bufPtr, uint32 bufWidth, unsigned int texX, unsigned int texY, unsigned int texWidth, unsigned int texHeight)
{
	CGsPixelFormats::CPixelIndexorPSMCT32 indexor(m_pRAM, bufPtr, bufWidth);

	auto dstPitch = gxm_texture_get_stride(texture.getHandle());
	auto dst = reinterpret_cast<uint32*>(gxm_texture_get_data(texture.getHandle()));
	dst += texX + (texY * dstPitch);

	for(unsigned int y = 0; y < texHeight; y++)
	{
		for(unsigned int x = 0; x < texWidth; x++)
		{
			dst[x] = indexor.GetPixel(texX + x, texY + y);
		}

		dst += dstPitch;
	}
}

template <typename IndexorType>
void CGSH_GXM::TexUpdater_Psm16(GxmTextureResource &texture, uint32 bufPtr, uint32 bufWidth, unsigned int texX, unsigned int texY, unsigned int texWidth, unsigned int texHeight)
{
	IndexorType indexor(m_pRAM, bufPtr, bufWidth);

	auto dstPitch = gxm_texture_get_stride(texture.getHandle());
	auto dst = reinterpret_cast<uint16*>(gxm_texture_get_data(texture.getHandle()));
	dst += texX + (texY * dstPitch);

	for(unsigned int y = 0; y < texHeight; y++)
	{
		for(unsigned int x = 0; x < texWidth; x++)
		{
			dst[x] = indexor.GetPixel(texX + x, texY + y);
		}

		dst += dstPitch;
	}
}

template <typename IndexorType>
void CGSH_GXM::TexUpdater_Psm48(GxmTextureResource &texture, uint32 bufPtr, uint32 bufWidth, unsigned int texX, unsigned int texY, unsigned int texWidth, unsigned int texHeight)
{
	IndexorType indexor(m_pRAM, bufPtr, bufWidth);

	auto dstPitch = gxm_texture_get_stride(texture.getHandle());
	auto dst = reinterpret_cast<uint8*>(gxm_texture_get_data(texture.getHandle()));
	dst += texX + (texY * dstPitch);

	for(unsigned int y = 0; y < texHeight; y++)
	{
		for(unsigned int x = 0; x < texWidth; x++)
		{
			uint8 pixel = indexor.GetPixel(texX + x, texY + y);
			dst[x] = pixel;
		}

		dst += dstPitch;
	}
}

template <uint32 shiftAmount, uint32 mask>
void CGSH_GXM::TexUpdater_Psm48H(GxmTextureResource &texture, uint32 bufPtr, uint32 bufWidth, unsigned int texX, unsigned int texY, unsigned int texWidth, unsigned int texHeight)
{
	CGsPixelFormats::CPixelIndexorPSMCT32 indexor(m_pRAM, bufPtr, bufWidth);

	auto dstPitch = gxm_texture_get_stride(texture.getHandle());
	auto dst = reinterpret_cast<uint8*>(gxm_texture_get_data(texture.getHandle()));
	dst += texX + (texY * dstPitch);

	for(unsigned int y = 0; y < texHeight; y++)
	{
		for(unsigned int x = 0; x < texWidth; x++)
		{
			uint32 pixel = indexor.GetPixel(texX + x, texY + y);
			pixel = (pixel >> shiftAmount) & mask;
			dst[x] = static_cast<uint8>(pixel);
		}

		dst += dstPitch;
	}
}

struct gxm_palette *CGSH_GXM::GetClutTexture(const TEX0& tex0)
{
	std::array<uint32, 256> convertedClut;
	MakeLinearCLUT(tex0, convertedClut);

	int entryCount = CGsPixelFormats::IsPsmIDTEX4(tex0.nPsm) ? 16 : 256;
	auto clutTexture = CGsPixelFormats::IsPsmIDTEX4(tex0.nPsm) ? &m_clutTexture4 : &m_clutTexture8;
	void *dst = (void *)gxm_palette_get_data(clutTexture);

	memcpy(dst, convertedClut.data(), entryCount * sizeof(uint32_t));

	return clutTexture;
}
