#include <psp2/gxm.h>
#include <psp2/ctrl.h>
#include <psp2/display.h>
#include <psp2/kernel/sysmem.h>
#include <psp2/kernel/threadmgr.h>
#include <cstdlib>
#include <cstring>
#include <climits>
#include <cassert>
#include "gxm.h"
#include "math_utils.h"

#define LOG(...) printf(__VA_ARGS__)

#define ALIGN(x, a) (((x) + ((a) - 1)) & ~((a) - 1))

#define GXM_TEXTURE_IMPLICIT_STRIDE 8

#define DOUBLE_BUFFERING	1

#define DISPLAY_WIDTH		960
#define DISPLAY_HEIGHT		544
#define DISPLAY_STRIDE		960
#define DISPLAY_BUFFER_COUNT	2
#define DISPLAY_COLOR_FORMAT	SCE_GXM_COLOR_FORMAT_A8B8G8R8
#define DISPLAY_PIXEL_FORMAT	SCE_DISPLAY_PIXELFORMAT_A8B8G8R8
#define MAX_PENDING_SWAPS	(DISPLAY_BUFFER_COUNT - 1)
#define VERTEX_BUFFER_ENTRIES	USHRT_MAX

struct clear_vertex {
	vector2f position;
};

struct display_queue_callback_data {
	void *addr;
};

static void reserve_vertex_default_uniform_data(const SceGxmProgramParameter *param,
	unsigned int component_count, const float *data);
static void reserve_fragment_default_uniform_data(const SceGxmProgramParameter *param,
	unsigned int component_count, const float *data);
static void *gpu_alloc_map(SceKernelMemBlockType type, SceGxmMemoryAttribFlags gpu_attrib, size_t size, SceUID *uid);
static void gpu_unmap_free(SceUID uid);
static void *gpu_vertex_usse_alloc_map(size_t size, SceUID *uid, unsigned int *usse_offset);
static void gpu_vertex_usse_unmap_free(SceUID uid);
static void *gpu_fragment_usse_alloc_map(size_t size, SceUID *uid, unsigned int *usse_offset);
static void gpu_fragment_usse_unmap_free(SceUID uid);
static void *shader_patcher_host_alloc_cb(void *user_data, unsigned int size);
static void shader_patcher_host_free_cb(void *user_data, void *mem);
static void display_queue_callback(const void *callbackData);

extern unsigned char _binary_clear_v_gxp_start;
extern unsigned char _binary_clear_f_gxp_start;
extern unsigned char _binary_color_v_gxp_start;
extern unsigned char _binary_color_f_gxp_start;
extern unsigned char _binary_texture_v_gxp_start;
extern unsigned char _binary_texture_f_gxp_start;

static const SceGxmProgram *const gxm_program_clear_v = (SceGxmProgram *)&_binary_clear_v_gxp_start;
static const SceGxmProgram *const gxm_program_clear_f = (SceGxmProgram *)&_binary_clear_f_gxp_start;
static const SceGxmProgram *const gxm_program_color_v = (SceGxmProgram *)&_binary_color_v_gxp_start;
static const SceGxmProgram *const gxm_program_color_f = (SceGxmProgram *)&_binary_color_f_gxp_start;
static const SceGxmProgram *const gxm_program_texture_v = (SceGxmProgram *)&_binary_texture_v_gxp_start;
static const SceGxmProgram *const gxm_program_texture_f = (SceGxmProgram *)&_binary_texture_f_gxp_start;

static SceGxmContext *gxm_context;
static SceUID vdm_ring_buffer_uid;
static void *vdm_ring_buffer_addr;
static SceUID vertex_ring_buffer_uid;
static void *vertex_ring_buffer_addr;
static SceUID fragment_ring_buffer_uid;
static void *fragment_ring_buffer_addr;
static SceUID fragment_usse_ring_buffer_uid;
static void *fragment_usse_ring_buffer_addr;
static SceGxmRenderTarget *gxm_render_target;
static SceGxmColorSurface gxm_color_surfaces[DISPLAY_BUFFER_COUNT];
static SceUID gxm_color_surfaces_uid[DISPLAY_BUFFER_COUNT];
static void *gxm_color_surfaces_addr[DISPLAY_BUFFER_COUNT];
static SceGxmSyncObject *gxm_sync_objects[DISPLAY_BUFFER_COUNT];
static unsigned int gxm_front_buffer_index;
static unsigned int gxm_back_buffer_index;
static SceUID gxm_depth_stencil_surface_uid;
static void *gxm_depth_stencil_surface_addr;
static SceGxmDepthStencilSurface gxm_depth_stencil_surface;
static SceGxmShaderPatcher *gxm_shader_patcher;
static SceUID gxm_shader_patcher_buffer_uid;
static void *gxm_shader_patcher_buffer_addr;
static SceUID gxm_shader_patcher_vertex_usse_uid;
static void *gxm_shader_patcher_vertex_usse_addr;
static SceUID gxm_shader_patcher_fragment_usse_uid;
static void *gxm_shader_patcher_fragment_usse_addr;

static SceGxmShaderPatcherId gxm_clear_vertex_program_id;
static SceGxmShaderPatcherId gxm_clear_fragment_program_id;
static const SceGxmProgramParameter *gxm_clear_vertex_program_position_param;
static const SceGxmProgramParameter *gxm_clear_fragment_program_u_clear_color_param;
static SceGxmVertexProgram *gxm_clear_vertex_program_patched;
static SceGxmFragmentProgram *gxm_clear_fragment_program_patched;

static SceGxmShaderPatcherId gxm_color_vertex_program_id;
static SceGxmShaderPatcherId gxm_color_fragment_program_id;
static const SceGxmProgramParameter *gxm_color_vertex_program_position_param;
static const SceGxmProgramParameter *gxm_color_vertex_program_color_param;
static SceGxmVertexProgram *gxm_color_vertex_program_patched;

static SceGxmShaderPatcherId gxm_texture_vertex_program_id;
static SceGxmShaderPatcherId gxm_texture_fragment_program_id;
static const SceGxmProgramParameter *gxm_texture_vertex_program_position_param;
static const SceGxmProgramParameter *gxm_texture_vertex_program_color_param;
static const SceGxmProgramParameter *gxm_texture_vertex_program_texcoord_param;
static SceGxmVertexProgram *gxm_texture_vertex_program_patched;

static SceUID clear_vertices_uid;
static SceUID clear_indices_uid;
struct clear_vertex *clear_vertices_addr;
static unsigned short *clear_indices_addr;

static SceUID vertex_buffer_color_uid;
static SceUID vertex_buffer_color_indices_uid;
static void *vertex_buffer_color_addr;
static unsigned short *vertex_buffer_color_indices_addr;

static SceUID vertex_buffer_texture_uid;
static SceUID vertex_buffer_texture_indices_uid;
static void *vertex_buffer_texture_addr;
static unsigned short *vertex_buffer_texture_indices_addr;

/* Global state */
static unsigned int vertex_buffer_color_size;
static unsigned int vertex_buffer_texture_size;

static inline uint32_t texture_format_bytes_per_pixel(SceGxmTextureFormat format)
{
	switch (format & 0x9f000000U) {
	case SCE_GXM_TEXTURE_BASE_FORMAT_U8:
	case SCE_GXM_TEXTURE_BASE_FORMAT_S8:
	case SCE_GXM_TEXTURE_BASE_FORMAT_P8:
		return 1;
	case SCE_GXM_TEXTURE_BASE_FORMAT_U4U4U4U4:
	case SCE_GXM_TEXTURE_BASE_FORMAT_U8U3U3U2:
	case SCE_GXM_TEXTURE_BASE_FORMAT_U1U5U5U5:
	case SCE_GXM_TEXTURE_BASE_FORMAT_U5U6U5:
	case SCE_GXM_TEXTURE_BASE_FORMAT_S5S5U6:
	case SCE_GXM_TEXTURE_BASE_FORMAT_U8U8:
	case SCE_GXM_TEXTURE_BASE_FORMAT_S8S8:
		return 2;
	case SCE_GXM_TEXTURE_BASE_FORMAT_U8U8U8:
	case SCE_GXM_TEXTURE_BASE_FORMAT_S8S8S8:
		return 3;
	case SCE_GXM_TEXTURE_BASE_FORMAT_U8U8U8U8:
	case SCE_GXM_TEXTURE_BASE_FORMAT_S8S8S8S8:
	case SCE_GXM_TEXTURE_BASE_FORMAT_F32:
	case SCE_GXM_TEXTURE_BASE_FORMAT_U32:
	case SCE_GXM_TEXTURE_BASE_FORMAT_S32:
	default:
		return 4;
	}
}

int gxm_init()
{
	SceGxmInitializeParams gxm_init_params;
	memset(&gxm_init_params, 0, sizeof(gxm_init_params));
	gxm_init_params.flags = 0;
	gxm_init_params.displayQueueMaxPendingCount = MAX_PENDING_SWAPS;
	gxm_init_params.displayQueueCallback = display_queue_callback;
	gxm_init_params.displayQueueCallbackDataSize = sizeof(struct display_queue_callback_data);
	gxm_init_params.parameterBufferSize = SCE_GXM_DEFAULT_PARAMETER_BUFFER_SIZE;

	sceGxmInitialize(&gxm_init_params);

	vdm_ring_buffer_addr = gpu_alloc_map(SCE_KERNEL_MEMBLOCK_TYPE_USER_RW_UNCACHE,
		SCE_GXM_MEMORY_ATTRIB_READ, SCE_GXM_DEFAULT_VDM_RING_BUFFER_SIZE,
		&vdm_ring_buffer_uid);

	vertex_ring_buffer_addr = gpu_alloc_map(SCE_KERNEL_MEMBLOCK_TYPE_USER_RW_UNCACHE,
		SCE_GXM_MEMORY_ATTRIB_READ, SCE_GXM_DEFAULT_VERTEX_RING_BUFFER_SIZE,
		&vertex_ring_buffer_uid);

	fragment_ring_buffer_addr = gpu_alloc_map(SCE_KERNEL_MEMBLOCK_TYPE_USER_RW_UNCACHE,
		SCE_GXM_MEMORY_ATTRIB_READ, SCE_GXM_DEFAULT_FRAGMENT_RING_BUFFER_SIZE,
		&fragment_ring_buffer_uid);

	unsigned int fragment_usse_offset;
	fragment_usse_ring_buffer_addr = gpu_fragment_usse_alloc_map(
		SCE_GXM_DEFAULT_FRAGMENT_USSE_RING_BUFFER_SIZE,
		&fragment_ring_buffer_uid, &fragment_usse_offset);

	SceGxmContextParams gxm_context_params;
	memset(&gxm_context_params, 0, sizeof(gxm_context_params));
	gxm_context_params.hostMem = malloc(SCE_GXM_MINIMUM_CONTEXT_HOST_MEM_SIZE);
	gxm_context_params.hostMemSize = SCE_GXM_MINIMUM_CONTEXT_HOST_MEM_SIZE;
	gxm_context_params.vdmRingBufferMem = vdm_ring_buffer_addr;
	gxm_context_params.vdmRingBufferMemSize = SCE_GXM_DEFAULT_VDM_RING_BUFFER_SIZE;
	gxm_context_params.vertexRingBufferMem = vertex_ring_buffer_addr;
	gxm_context_params.vertexRingBufferMemSize = SCE_GXM_DEFAULT_VERTEX_RING_BUFFER_SIZE;
	gxm_context_params.fragmentRingBufferMem = fragment_ring_buffer_addr;
	gxm_context_params.fragmentRingBufferMemSize = SCE_GXM_DEFAULT_FRAGMENT_RING_BUFFER_SIZE;
	gxm_context_params.fragmentUsseRingBufferMem = fragment_usse_ring_buffer_addr;
	gxm_context_params.fragmentUsseRingBufferMemSize = SCE_GXM_DEFAULT_FRAGMENT_USSE_RING_BUFFER_SIZE;
	gxm_context_params.fragmentUsseRingBufferOffset = fragment_usse_offset;

	sceGxmCreateContext(&gxm_context_params, &gxm_context);

	SceGxmRenderTargetParams render_target_params;
	memset(&render_target_params, 0, sizeof(render_target_params));
	render_target_params.flags = 0;
	render_target_params.width = DISPLAY_WIDTH;
	render_target_params.height = DISPLAY_HEIGHT;
	render_target_params.scenesPerFrame = 1;
	render_target_params.multisampleMode = SCE_GXM_MULTISAMPLE_NONE;
	render_target_params.multisampleLocations = 0;
	render_target_params.driverMemBlock = -1;

	sceGxmCreateRenderTarget(&render_target_params, &gxm_render_target);

	for (int i = 0; i < DISPLAY_BUFFER_COUNT; i++) {
		gxm_color_surfaces_addr[i] = gpu_alloc_map(SCE_KERNEL_MEMBLOCK_TYPE_USER_CDRAM_RW,
			SCE_GXM_MEMORY_ATTRIB_RW,
			ALIGN(4 * DISPLAY_STRIDE * DISPLAY_HEIGHT, 1 * 1024 * 1024),
			&gxm_color_surfaces_uid[i]);

		memset(gxm_color_surfaces_addr[i], 0, DISPLAY_STRIDE * DISPLAY_HEIGHT);

		sceGxmColorSurfaceInit(&gxm_color_surfaces[i],
			DISPLAY_COLOR_FORMAT,
			SCE_GXM_COLOR_SURFACE_LINEAR,
			SCE_GXM_COLOR_SURFACE_SCALE_NONE,
			SCE_GXM_OUTPUT_REGISTER_SIZE_32BIT,
			DISPLAY_WIDTH,
			DISPLAY_HEIGHT,
			DISPLAY_STRIDE,
			gxm_color_surfaces_addr[i]);

		sceGxmSyncObjectCreate(&gxm_sync_objects[i]);
	}

	unsigned int depth_stencil_width = ALIGN(DISPLAY_WIDTH, SCE_GXM_TILE_SIZEX);
	unsigned int depth_stencil_height = ALIGN(DISPLAY_HEIGHT, SCE_GXM_TILE_SIZEY);
	unsigned int depth_stencil_samples = depth_stencil_width * depth_stencil_height;

	gxm_depth_stencil_surface_addr = gpu_alloc_map(SCE_KERNEL_MEMBLOCK_TYPE_USER_CDRAM_RW,
		SCE_GXM_MEMORY_ATTRIB_RW,
		4 * depth_stencil_samples, &gxm_depth_stencil_surface_uid);

	sceGxmDepthStencilSurfaceInit(&gxm_depth_stencil_surface,
		SCE_GXM_DEPTH_STENCIL_FORMAT_S8D24,
		SCE_GXM_DEPTH_STENCIL_SURFACE_TILED,
		depth_stencil_width,
		gxm_depth_stencil_surface_addr,
		NULL);

	static const unsigned int shader_patcher_buffer_size = 64 * 1024;
	static const unsigned int shader_patcher_vertex_usse_size = 64 * 1024;
	static const unsigned int shader_patcher_fragment_usse_size = 64 * 1024;

	gxm_shader_patcher_buffer_addr = gpu_alloc_map(SCE_KERNEL_MEMBLOCK_TYPE_USER_CDRAM_RW,
		SCE_GXM_MEMORY_ATTRIB_RW,
		shader_patcher_buffer_size, &gxm_shader_patcher_buffer_uid);

	unsigned int shader_patcher_vertex_usse_offset;
	gxm_shader_patcher_vertex_usse_addr = gpu_vertex_usse_alloc_map(
		shader_patcher_vertex_usse_size, &gxm_shader_patcher_vertex_usse_uid,
		&shader_patcher_vertex_usse_offset);

	unsigned int shader_patcher_fragment_usse_offset;
	gxm_shader_patcher_fragment_usse_addr = gpu_fragment_usse_alloc_map(
		shader_patcher_fragment_usse_size, &gxm_shader_patcher_fragment_usse_uid,
		&shader_patcher_fragment_usse_offset);

	SceGxmShaderPatcherParams shader_patcher_params;
	memset(&shader_patcher_params, 0, sizeof(shader_patcher_params));
	shader_patcher_params.userData = NULL;
	shader_patcher_params.hostAllocCallback = shader_patcher_host_alloc_cb;
	shader_patcher_params.hostFreeCallback = shader_patcher_host_free_cb;
	shader_patcher_params.bufferAllocCallback = NULL;
	shader_patcher_params.bufferFreeCallback = NULL;
	shader_patcher_params.bufferMem = gxm_shader_patcher_buffer_addr;
	shader_patcher_params.bufferMemSize = shader_patcher_buffer_size;
	shader_patcher_params.vertexUsseAllocCallback = NULL;
	shader_patcher_params.vertexUsseFreeCallback = NULL;
	shader_patcher_params.vertexUsseMem = gxm_shader_patcher_vertex_usse_addr;
	shader_patcher_params.vertexUsseMemSize = shader_patcher_vertex_usse_size;
	shader_patcher_params.vertexUsseOffset = shader_patcher_vertex_usse_offset;
	shader_patcher_params.fragmentUsseAllocCallback = NULL;
	shader_patcher_params.fragmentUsseFreeCallback = NULL;
	shader_patcher_params.fragmentUsseMem = gxm_shader_patcher_fragment_usse_addr;
	shader_patcher_params.fragmentUsseMemSize = shader_patcher_fragment_usse_size;
	shader_patcher_params.fragmentUsseOffset = shader_patcher_fragment_usse_offset;

	sceGxmShaderPatcherCreate(&shader_patcher_params, &gxm_shader_patcher);

	/* Clear program */
	sceGxmShaderPatcherRegisterProgram(gxm_shader_patcher, gxm_program_clear_v,
		&gxm_clear_vertex_program_id);
	sceGxmShaderPatcherRegisterProgram(gxm_shader_patcher, gxm_program_clear_f,
		&gxm_clear_fragment_program_id);

	gxm_clear_vertex_program_position_param = sceGxmProgramFindParameterByName(
		gxm_program_clear_v, "position");

	gxm_clear_fragment_program_u_clear_color_param = sceGxmProgramFindParameterByName(
		gxm_program_clear_f, "u_clear_color");

	SceGxmVertexAttribute clear_vertex_attribute;
	SceGxmVertexStream clear_vertex_stream;
	clear_vertex_attribute.streamIndex = 0;
	clear_vertex_attribute.offset = 0;
	clear_vertex_attribute.format = SCE_GXM_ATTRIBUTE_FORMAT_F32;
	clear_vertex_attribute.componentCount = 2;
	clear_vertex_attribute.regIndex = sceGxmProgramParameterGetResourceIndex(
		gxm_clear_vertex_program_position_param);
	clear_vertex_stream.stride = sizeof(struct clear_vertex);
	clear_vertex_stream.indexSource = SCE_GXM_INDEX_SOURCE_INDEX_16BIT;

	sceGxmShaderPatcherCreateVertexProgram(gxm_shader_patcher,
		gxm_clear_vertex_program_id, &clear_vertex_attribute,
		1, &clear_vertex_stream, 1, &gxm_clear_vertex_program_patched);

	sceGxmShaderPatcherCreateFragmentProgram(gxm_shader_patcher,
		gxm_clear_fragment_program_id, SCE_GXM_OUTPUT_REGISTER_FORMAT_UCHAR4,
		SCE_GXM_MULTISAMPLE_NONE, NULL, gxm_program_clear_v,
		&gxm_clear_fragment_program_patched);

	clear_vertices_addr = (struct clear_vertex *)gpu_alloc_map(
		SCE_KERNEL_MEMBLOCK_TYPE_USER_RW, SCE_GXM_MEMORY_ATTRIB_READ,
		4 * sizeof(struct clear_vertex), &clear_vertices_uid);

	clear_indices_addr = (unsigned short *)gpu_alloc_map(
		SCE_KERNEL_MEMBLOCK_TYPE_USER_RW, SCE_GXM_MEMORY_ATTRIB_READ,
		4 * sizeof(unsigned short), &clear_indices_uid);

	clear_vertices_addr[0].position = (vector2f){-1.0f, -1.0f};
	clear_vertices_addr[1].position = (vector2f){ 1.0f, -1.0f};
	clear_vertices_addr[2].position = (vector2f){-1.0f,  1.0f};
	clear_vertices_addr[3].position = (vector2f){ 1.0f,  1.0f};

	clear_indices_addr[0] = 0;
	clear_indices_addr[1] = 1;
	clear_indices_addr[2] = 2;
	clear_indices_addr[3] = 3;

	/* Color program */
	sceGxmShaderPatcherRegisterProgram(gxm_shader_patcher, gxm_program_color_v,
		&gxm_color_vertex_program_id);

	sceGxmShaderPatcherRegisterProgram(gxm_shader_patcher, gxm_program_color_f,
		&gxm_color_fragment_program_id);

	gxm_color_vertex_program_position_param = sceGxmProgramFindParameterByName(
		gxm_program_color_v, "position");

	gxm_color_vertex_program_color_param = sceGxmProgramFindParameterByName(
		gxm_program_color_v, "color");

	SceGxmVertexAttribute color_vertex_attributes[2];
	SceGxmVertexStream color_vertex_stream;
	color_vertex_attributes[0].streamIndex = 0;
	color_vertex_attributes[0].offset = offsetof(struct color_vertex, position);
	color_vertex_attributes[0].format = SCE_GXM_ATTRIBUTE_FORMAT_F32;
	color_vertex_attributes[0].componentCount = 3;
	color_vertex_attributes[0].regIndex = sceGxmProgramParameterGetResourceIndex(
		gxm_color_vertex_program_position_param);
	color_vertex_attributes[1].streamIndex = 0;
	color_vertex_attributes[1].offset = offsetof(struct color_vertex, color);
	color_vertex_attributes[1].format = SCE_GXM_ATTRIBUTE_FORMAT_U8N;
	color_vertex_attributes[1].componentCount = 4;
	color_vertex_attributes[1].regIndex = sceGxmProgramParameterGetResourceIndex(
		gxm_color_vertex_program_color_param);
	color_vertex_stream.stride = sizeof(struct color_vertex);
	color_vertex_stream.indexSource = SCE_GXM_INDEX_SOURCE_INDEX_16BIT;

	sceGxmShaderPatcherCreateVertexProgram(gxm_shader_patcher,
		gxm_color_vertex_program_id, color_vertex_attributes,
		ARRAY_SIZE(color_vertex_attributes), &color_vertex_stream,
		1, &gxm_color_vertex_program_patched);

	/* Texture program */
	sceGxmShaderPatcherRegisterProgram(gxm_shader_patcher, gxm_program_texture_v,
		&gxm_texture_vertex_program_id);

	sceGxmShaderPatcherRegisterProgram(gxm_shader_patcher, gxm_program_texture_f,
		&gxm_texture_fragment_program_id);

	gxm_texture_vertex_program_position_param = sceGxmProgramFindParameterByName(
		gxm_program_texture_v, "position");

	gxm_texture_vertex_program_color_param = sceGxmProgramFindParameterByName(
		gxm_program_texture_v, "color");

	gxm_texture_vertex_program_texcoord_param = sceGxmProgramFindParameterByName(
		gxm_program_texture_v, "texcoord");

	SceGxmVertexAttribute texture_vertex_attributes[3];
	SceGxmVertexStream texture_vertex_stream;
	texture_vertex_attributes[0].streamIndex = 0;
	texture_vertex_attributes[0].offset = offsetof(struct texture_vertex, position);
	texture_vertex_attributes[0].format = SCE_GXM_ATTRIBUTE_FORMAT_F32;
	texture_vertex_attributes[0].componentCount = 3;
	texture_vertex_attributes[0].regIndex = sceGxmProgramParameterGetResourceIndex(
		gxm_texture_vertex_program_position_param);
	texture_vertex_attributes[1].streamIndex = 0;
	texture_vertex_attributes[1].offset = offsetof(struct texture_vertex, color);
	texture_vertex_attributes[1].format = SCE_GXM_ATTRIBUTE_FORMAT_U8N;
	texture_vertex_attributes[1].componentCount = 4;
	texture_vertex_attributes[1].regIndex = sceGxmProgramParameterGetResourceIndex(
		gxm_texture_vertex_program_color_param);
	texture_vertex_attributes[2].streamIndex = 0;
	texture_vertex_attributes[2].offset = offsetof(struct texture_vertex, texcoord);
	texture_vertex_attributes[2].format = SCE_GXM_ATTRIBUTE_FORMAT_F32;
	texture_vertex_attributes[2].componentCount = 2;
	texture_vertex_attributes[2].regIndex = sceGxmProgramParameterGetResourceIndex(
		gxm_texture_vertex_program_texcoord_param);
	texture_vertex_stream.stride = sizeof(struct texture_vertex);
	texture_vertex_stream.indexSource = SCE_GXM_INDEX_SOURCE_INDEX_16BIT;

	sceGxmShaderPatcherCreateVertexProgram(gxm_shader_patcher,
		gxm_texture_vertex_program_id, texture_vertex_attributes,
		ARRAY_SIZE(texture_vertex_attributes), &texture_vertex_stream,
		1, &gxm_texture_vertex_program_patched);

	/* Vertex buffer for colored vertices */
	vertex_buffer_color_addr = gpu_alloc_map(
		SCE_KERNEL_MEMBLOCK_TYPE_USER_RW, SCE_GXM_MEMORY_ATTRIB_READ,
		VERTEX_BUFFER_ENTRIES * sizeof(struct color_vertex), &vertex_buffer_color_uid);

	vertex_buffer_color_indices_addr = (unsigned short *)gpu_alloc_map(
		SCE_KERNEL_MEMBLOCK_TYPE_USER_RW, SCE_GXM_MEMORY_ATTRIB_READ,
		VERTEX_BUFFER_ENTRIES * sizeof(unsigned short), &vertex_buffer_color_indices_uid);

	/* Vertex buffer for textured vertices */
	vertex_buffer_texture_addr = gpu_alloc_map(
		SCE_KERNEL_MEMBLOCK_TYPE_USER_RW, SCE_GXM_MEMORY_ATTRIB_READ,
		VERTEX_BUFFER_ENTRIES * sizeof(struct texture_vertex), &vertex_buffer_texture_uid);

	vertex_buffer_texture_indices_addr = (unsigned short *)gpu_alloc_map(
		SCE_KERNEL_MEMBLOCK_TYPE_USER_RW, SCE_GXM_MEMORY_ATTRIB_READ,
		VERTEX_BUFFER_ENTRIES * sizeof(unsigned short), &vertex_buffer_texture_indices_uid);

	for (int i = 0; i < VERTEX_BUFFER_ENTRIES; i++) {
		vertex_buffer_color_indices_addr[i] = i;
		vertex_buffer_texture_indices_addr[i] = i;
	}

	/* Global state */
	vertex_buffer_color_size = 0;
	vertex_buffer_texture_size = 0;

	/* Initial GXM/display state */

#if !DOUBLE_BUFFERING
	// Single buffering, display and render directly to the back buffer
	SceDisplayFrameBuf display_fb;
	memset(&display_fb, 0, sizeof(display_fb));
	display_fb.size = sizeof(display_fb);
	display_fb.base = gxm_color_surfaces_addr[gxm_back_buffer_index];
	display_fb.pitch = DISPLAY_STRIDE;
	display_fb.pixelformat = DISPLAY_PIXEL_FORMAT;
	display_fb.width = DISPLAY_WIDTH;
	display_fb.height = DISPLAY_HEIGHT;
	sceDisplaySetFrameBuf(&display_fb, SCE_DISPLAY_SETBUF_NEXTFRAME);
#endif

	return 0;
}

int gxm_finish()
{
	sceGxmFinish(gxm_context);
	return 0;
}

int gxm_terminate()
{
	sceGxmDisplayQueueFinish();
	sceGxmFinish(gxm_context);

	gpu_unmap_free(vertex_buffer_color_uid);
	gpu_unmap_free(vertex_buffer_color_indices_uid);
	gpu_unmap_free(vertex_buffer_texture_uid);
	gpu_unmap_free(vertex_buffer_texture_indices_uid);
	gpu_unmap_free(clear_vertices_uid);
	gpu_unmap_free(clear_indices_uid);

	sceGxmShaderPatcherReleaseVertexProgram(gxm_shader_patcher,
		gxm_clear_vertex_program_patched);
	sceGxmShaderPatcherReleaseFragmentProgram(gxm_shader_patcher,
		gxm_clear_fragment_program_patched);

	sceGxmShaderPatcherReleaseVertexProgram(gxm_shader_patcher,
		gxm_color_vertex_program_patched);

	sceGxmShaderPatcherReleaseVertexProgram(gxm_shader_patcher,
		gxm_texture_vertex_program_patched);

	sceGxmShaderPatcherUnregisterProgram(gxm_shader_patcher,
		gxm_clear_vertex_program_id);
	sceGxmShaderPatcherUnregisterProgram(gxm_shader_patcher,
		gxm_clear_fragment_program_id);

	sceGxmShaderPatcherUnregisterProgram(gxm_shader_patcher,
		gxm_color_vertex_program_id);
	sceGxmShaderPatcherUnregisterProgram(gxm_shader_patcher,
		gxm_color_fragment_program_id);

	sceGxmShaderPatcherUnregisterProgram(gxm_shader_patcher,
		gxm_texture_vertex_program_id);
	sceGxmShaderPatcherUnregisterProgram(gxm_shader_patcher,
		gxm_texture_fragment_program_id);

	sceGxmShaderPatcherDestroy(gxm_shader_patcher);

	gpu_unmap_free(gxm_shader_patcher_buffer_uid);
	gpu_vertex_usse_unmap_free(gxm_shader_patcher_vertex_usse_uid);
	gpu_fragment_usse_unmap_free(gxm_shader_patcher_fragment_usse_uid);

	gpu_unmap_free(gxm_depth_stencil_surface_uid);

	for (int i = 0; i < DISPLAY_BUFFER_COUNT; i++) {
		gpu_unmap_free(gxm_color_surfaces_uid[i]);
		sceGxmSyncObjectDestroy(gxm_sync_objects[i]);
	}

	sceGxmDestroyRenderTarget(gxm_render_target);

	gpu_unmap_free(vdm_ring_buffer_uid);
	gpu_unmap_free(vertex_ring_buffer_uid);
	gpu_unmap_free(fragment_ring_buffer_uid);
	gpu_fragment_usse_unmap_free(fragment_usse_ring_buffer_uid);

	sceGxmDestroyContext(gxm_context);

	sceGxmTerminate();

	return 0;
}

static void gxm_clear_depth_stencil()
{
	sceGxmBeginScene(gxm_context,
		0,
		gxm_render_target,
		NULL,
		NULL,
		gxm_sync_objects[gxm_back_buffer_index],
		&gxm_color_surfaces[gxm_back_buffer_index],
		&gxm_depth_stencil_surface);

	sceGxmSetViewportEnable(gxm_context, SCE_GXM_VIEWPORT_ENABLED);
	sceGxmSetFrontDepthFunc(gxm_context, SCE_GXM_DEPTH_FUNC_ALWAYS);
	sceGxmSetFrontDepthWriteEnable(gxm_context, SCE_GXM_DEPTH_WRITE_ENABLED);
	sceGxmSetFrontStencilFunc(gxm_context,
		SCE_GXM_STENCIL_FUNC_ALWAYS,
		SCE_GXM_STENCIL_OP_ZERO,
		SCE_GXM_STENCIL_OP_ZERO,
		SCE_GXM_STENCIL_OP_ZERO,
		0, 0xFF);

	sceGxmSetVertexProgram(gxm_context, gxm_clear_vertex_program_patched);
	sceGxmSetFragmentProgram(gxm_context, gxm_clear_fragment_program_patched);

	static const float clear_color[4] = {
		1.0f, 1.0f, 1.0f, 1.0f
	};

	reserve_fragment_default_uniform_data(gxm_clear_fragment_program_u_clear_color_param,
		sizeof(clear_color) / sizeof(float), clear_color);

	sceGxmSetVertexStream(gxm_context, 0, clear_vertices_addr);
	sceGxmDraw(gxm_context, SCE_GXM_PRIMITIVE_TRIANGLE_STRIP,
		SCE_GXM_INDEX_FORMAT_U16, clear_indices_addr, 4);

	sceGxmEndScene(gxm_context, NULL, NULL);
}

void gxm_begin_scene()
{
	sceGxmBeginScene(gxm_context,
		0,
		gxm_render_target,
		NULL,
		NULL,
		gxm_sync_objects[gxm_back_buffer_index],
		&gxm_color_surfaces[gxm_back_buffer_index],
		&gxm_depth_stencil_surface);

	/* Vertices coming from GS are already in window coordinates! */
	sceGxmSetViewportEnable(gxm_context, SCE_GXM_VIEWPORT_DISABLED);
}

void gxm_end_scene()
{
	sceGxmEndScene(gxm_context, NULL, NULL);
}

void gxm_queue_flip()
{
	sceGxmPadHeartbeat(&gxm_color_surfaces[gxm_back_buffer_index],
		gxm_sync_objects[gxm_back_buffer_index]);

	struct display_queue_callback_data queue_cb_data;
	queue_cb_data.addr = gxm_color_surfaces_addr[gxm_back_buffer_index];

	sceGxmDisplayQueueAddEntry(gxm_sync_objects[gxm_front_buffer_index],
		gxm_sync_objects[gxm_back_buffer_index], &queue_cb_data);

#if DOUBLE_BUFFERING
	gxm_front_buffer_index = gxm_back_buffer_index;
	gxm_back_buffer_index = (gxm_back_buffer_index + 1) % DISPLAY_BUFFER_COUNT;
#endif
}

void gxm_color_surfaces_set_clip(unsigned int x_min, unsigned int y_min, unsigned int x_max, unsigned int y_max)
{
	for (int i = 0; i < DISPLAY_BUFFER_COUNT; i++)
		sceGxmColorSurfaceSetClip(&gxm_color_surfaces[i], x_min, y_min, x_max, y_max);
}

void gxm_set_color_fragment_program(const SceGxmFragmentProgram *fragment_program)
{
	sceGxmSetFragmentProgram(gxm_context, fragment_program);
	// TODO
	sceGxmSetVertexProgram(gxm_context, gxm_color_vertex_program_patched);
	sceGxmSetVertexStream(gxm_context, 0, vertex_buffer_color_addr);
}

void gxm_set_texture_fragment_program(const SceGxmFragmentProgram *fragment_program)
{
	sceGxmSetFragmentProgram(gxm_context, fragment_program);
	// TODO
	sceGxmSetVertexProgram(gxm_context, gxm_texture_vertex_program_patched);
	sceGxmSetVertexStream(gxm_context, 0, vertex_buffer_texture_addr);
}

void gxm_create_color_fragment_program(const SceGxmBlendInfo *blend_info, SceGxmFragmentProgram **fragment_program)
{
	sceGxmShaderPatcherCreateFragmentProgram(gxm_shader_patcher,
		gxm_color_fragment_program_id, SCE_GXM_OUTPUT_REGISTER_FORMAT_UCHAR4,
		SCE_GXM_MULTISAMPLE_NONE, blend_info, gxm_program_color_v,
		fragment_program);
}

void gxm_create_texture_fragment_program(const SceGxmBlendInfo *blend_info, SceGxmFragmentProgram **fragment_program)
{
	sceGxmShaderPatcherCreateFragmentProgram(gxm_shader_patcher,
		gxm_texture_fragment_program_id, SCE_GXM_OUTPUT_REGISTER_FORMAT_UCHAR4,
		SCE_GXM_MULTISAMPLE_NONE, blend_info, gxm_program_texture_v,
		fragment_program);
}

void gxm_release_fragment_program(SceGxmFragmentProgram *fragment_program)
{
	sceGxmShaderPatcherReleaseFragmentProgram(gxm_shader_patcher, fragment_program);
}

void gxm_create_texture(struct gxm_texture *texture, uint32_t width, uint32_t height, SceGxmTextureFormat format)
{
	void *data;
	uint32_t stride = ALIGN(width, GXM_TEXTURE_IMPLICIT_STRIDE);
	uint32_t size = stride * height * texture_format_bytes_per_pixel(format);

	gpu_alloc_map(SCE_KERNEL_MEMBLOCK_TYPE_USER_CDRAM_RW,
		SCE_GXM_MEMORY_ATTRIB_READ, size, &texture->data_uid);

	sceKernelGetMemBlockBase(texture->data_uid, &data);

	sceGxmTextureInitLinear(&texture->texture, data, format, width, height, 0);
	texture->stride = stride;
}

void gxm_texture_set_palette(struct gxm_texture *texture, const struct gxm_palette *palette)
{
	sceGxmTextureSetPalette(&texture->texture, palette->data);
}

void gxm_texture_set_fragment_texture(const struct gxm_texture *texture, uint32_t texture_index)
{
	sceGxmSetFragmentTexture(gxm_context, texture_index, &texture->texture);
}

void *gxm_texture_get_data(const struct gxm_texture *texture)
{
	return sceGxmTextureGetData(&texture->texture);
}

uint32_t gxm_texture_get_width(const struct gxm_texture *texture)
{
	return sceGxmTextureGetWidth(&texture->texture);
}

uint32_t gxm_texture_get_height(const struct gxm_texture *texture)
{
	return sceGxmTextureGetHeight(&texture->texture);
}

uint32_t gxm_texture_get_stride(const struct gxm_texture *texture)
{
	return texture->stride;
}

void gxm_texture_set_min_filter(struct gxm_texture *texture, SceGxmTextureFilter filter)
{
	sceGxmTextureSetMinFilter(&texture->texture, filter);
}

void gxm_texture_set_mag_filter(struct gxm_texture *texture, SceGxmTextureFilter filter)
{
	sceGxmTextureSetMagFilter(&texture->texture, filter);
}

void gxm_texture_set_mip_filter(struct gxm_texture *texture, SceGxmTextureMipFilter filter)
{
	sceGxmTextureSetMipFilter(&texture->texture, filter);
}

void gxm_texture_set_u_addr_mode(struct gxm_texture *texture, SceGxmTextureAddrMode mode)
{
	sceGxmTextureSetUAddrMode(&texture->texture, mode);
}

void gxm_texture_set_v_addr_mode(struct gxm_texture *texture, SceGxmTextureAddrMode mode)
{
	sceGxmTextureSetVAddrMode(&texture->texture, mode);
}

void gxm_free_texture(struct gxm_texture *texture)
{
	gpu_unmap_free(texture->data_uid);
}

void gxm_create_palette(struct gxm_palette *palette, SceGxmTextureBaseFormat base_format)
{
	uint32_t size;

	switch (base_format) {
	case SCE_GXM_TEXTURE_BASE_FORMAT_P4:
		size = 16 * sizeof(uint32_t);
		break;
	case SCE_GXM_TEXTURE_BASE_FORMAT_P8:
		size = 256 * sizeof(uint32_t);
		break;
	default:
		assert(false);
		break;
	}

	gpu_alloc_map(SCE_KERNEL_MEMBLOCK_TYPE_USER_CDRAM_RW,
		SCE_GXM_MEMORY_ATTRIB_READ, size, &palette->data_uid);

	sceKernelGetMemBlockBase(palette->data_uid, (void **)&palette->data);
}

uint32_t *gxm_palette_get_data(struct gxm_palette *palette)
{
	return palette->data;
}
void gxm_free_palette(struct gxm_palette *palette)
{
	gpu_unmap_free(palette->data_uid);
}

void gxm_vertex_buffer_color_push(SceGxmPrimitiveType prim, const struct color_vertex *vertices, uint32_t count)
{
	struct color_vertex *buffer = (struct color_vertex *)vertex_buffer_color_addr;
	uint32_t head = vertex_buffer_color_size;

	assert(vertex_buffer_color_size + count <= VERTEX_BUFFER_ENTRIES);
	vertex_buffer_color_size += count;

	memcpy(&buffer[head], vertices, count * sizeof(vertices[0]));
	sceGxmDraw(gxm_context, prim, SCE_GXM_INDEX_FORMAT_U16,
		&vertex_buffer_color_indices_addr[head], count);
}

void gxm_vertex_buffer_texture_push(SceGxmPrimitiveType prim, const struct texture_vertex *vertices, uint32_t count)
{
	struct texture_vertex *buffer = (struct texture_vertex *)vertex_buffer_texture_addr;
	uint32_t head = vertex_buffer_texture_size;

	assert(vertex_buffer_texture_size + count <= VERTEX_BUFFER_ENTRIES);
	vertex_buffer_texture_size += count;

	memcpy(&buffer[head], vertices, count * sizeof(vertices[0]));
	sceGxmDraw(gxm_context, prim, SCE_GXM_INDEX_FORMAT_U16,
		&vertex_buffer_texture_indices_addr[head], count);
}

void gxm_vertex_buffer_color_reset()
{
	vertex_buffer_color_size = 0;
}

void gxm_vertex_buffer_texture_reset()
{
	vertex_buffer_texture_size = 0;
}

void gxm_set_polygon_mode(SceGxmPolygonMode mode)
{
	sceGxmSetFrontPolygonMode(gxm_context, mode);
}

void gxm_set_depth_write_mode(SceGxmDepthWriteMode mode)
{
	sceGxmSetFrontDepthWriteEnable(gxm_context, mode);
}

void gxm_set_depth_test_function(SceGxmDepthFunc func)
{
	sceGxmSetFrontDepthFunc(gxm_context, func);
}

void reserve_vertex_default_uniform_data(const SceGxmProgramParameter *param,
	unsigned int component_count, const float *data)
{
	void *uniform_buffer;
	sceGxmReserveVertexDefaultUniformBuffer(gxm_context, &uniform_buffer);
	sceGxmSetUniformDataF(uniform_buffer, param, 0, component_count, data);
}

void reserve_fragment_default_uniform_data(const SceGxmProgramParameter *param,
	unsigned int component_count, const float *data)
{
	void *uniform_buffer;
	sceGxmReserveFragmentDefaultUniformBuffer(gxm_context, &uniform_buffer);
	sceGxmSetUniformDataF(uniform_buffer, param, 0, component_count, data);
}

void *gpu_alloc_map(SceKernelMemBlockType type, SceGxmMemoryAttribFlags gpu_attrib, size_t size, SceUID *uid)
{
	SceUID memuid;
	void *addr;

	if (type == SCE_KERNEL_MEMBLOCK_TYPE_USER_CDRAM_RW)
		size = ALIGN(size, 256 * 1024);
	else
		size = ALIGN(size, 4 * 1024);

	memuid = sceKernelAllocMemBlock("gpumem", type, size, NULL);
	if (memuid < 0)
		return NULL;

	if (sceKernelGetMemBlockBase(memuid, &addr) < 0)
		return NULL;

	if (sceGxmMapMemory(addr, size, gpu_attrib) < 0) {
		sceKernelFreeMemBlock(memuid);
		return NULL;
	}

	if (uid)
		*uid = memuid;

	return addr;
}

void gpu_unmap_free(SceUID uid)
{
	void *addr;

	if (sceKernelGetMemBlockBase(uid, &addr) < 0)
		return;

	sceGxmUnmapMemory(addr);

	sceKernelFreeMemBlock(uid);
}

void *gpu_vertex_usse_alloc_map(size_t size, SceUID *uid, unsigned int *usse_offset)
{
	SceUID memuid;
	void *addr;

	size = ALIGN(size, 4 * 1024);

	memuid = sceKernelAllocMemBlock("gpu_vertex_usse",
		SCE_KERNEL_MEMBLOCK_TYPE_USER_RW_UNCACHE, size, NULL);
	if (memuid < 0)
		return NULL;

	if (sceKernelGetMemBlockBase(memuid, &addr) < 0)
		return NULL;

	if (sceGxmMapVertexUsseMemory(addr, size, usse_offset) < 0)
		return NULL;

	return addr;
}

void gpu_vertex_usse_unmap_free(SceUID uid)
{
	void *addr;

	if (sceKernelGetMemBlockBase(uid, &addr) < 0)
		return;

	sceGxmUnmapVertexUsseMemory(addr);

	sceKernelFreeMemBlock(uid);
}

void *gpu_fragment_usse_alloc_map(size_t size, SceUID *uid, unsigned int *usse_offset)
{
	SceUID memuid;
	void *addr;

	size = ALIGN(size, 4 * 1024);

	memuid = sceKernelAllocMemBlock("gpu_fragment_usse",
		SCE_KERNEL_MEMBLOCK_TYPE_USER_RW_UNCACHE, size, NULL);
	if (memuid < 0)
		return NULL;

	if (sceKernelGetMemBlockBase(memuid, &addr) < 0)
		return NULL;

	if (sceGxmMapFragmentUsseMemory(addr, size, usse_offset) < 0)
		return NULL;

	return addr;
}

void gpu_fragment_usse_unmap_free(SceUID uid)
{
	void *addr;

	if (sceKernelGetMemBlockBase(uid, &addr) < 0)
		return;

	sceGxmUnmapFragmentUsseMemory(addr);

	sceKernelFreeMemBlock(uid);
}

void *shader_patcher_host_alloc_cb(void *user_data, unsigned int size)
{
	return malloc(size);
}

void shader_patcher_host_free_cb(void *user_data, void *mem)
{
	return free(mem);
}

void display_queue_callback(const void *callbackData)
{
	SceDisplayFrameBuf display_fb;
	const struct display_queue_callback_data *cb_data =
		(const struct display_queue_callback_data *)callbackData;

	//LOG("GXM display_queue_callback\n");

	memset(&display_fb, 0, sizeof(display_fb));
	display_fb.size = sizeof(display_fb);
	display_fb.base = cb_data->addr;
	display_fb.pitch = DISPLAY_STRIDE;
	display_fb.pixelformat = DISPLAY_PIXEL_FORMAT;
	display_fb.width = DISPLAY_WIDTH;
	display_fb.height = DISPLAY_HEIGHT;

	sceDisplaySetFrameBuf(&display_fb, SCE_DISPLAY_SETBUF_NEXTFRAME);

	// No vsync for now :)
	// sceDisplayWaitVblankStart();
}
