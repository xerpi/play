#pragma once

#include "gxm.h"

class GxmTextureResource
{
public:
	GxmTextureResource() = default;
	GxmTextureResource(const GxmTextureResource&) = delete;

	GxmTextureResource(GxmTextureResource&& rhs)
	{
		MoveFrom(std::move(rhs));
	}

	~GxmTextureResource()
	{
		Reset();
	}

	static GxmTextureResource Create(uint32_t width, uint32_t height, SceGxmTextureFormat format)
	{
		GxmTextureResource resource;
		resource.m_handle = (struct gxm_texture *)malloc(sizeof(*resource.m_handle));
		gxm_create_texture(resource.m_handle, width, height, format);
		return resource;
	}

	GxmTextureResource& operator =(const GxmTextureResource&) = delete;

	GxmTextureResource& operator =(GxmTextureResource&& rhs)
	{
		Reset();
		MoveFrom(std::move(rhs));
		return (*this);
	}

	struct gxm_texture *getHandle()
	{
		return m_handle;
	}

	operator struct gxm_texture *() const
	{
		return m_handle;
	}

	void Reset()
	{
		if (m_handle != nullptr)
			gxm_free_texture(m_handle);
		m_handle = nullptr;
	}

private:
	void MoveFrom(GxmTextureResource&& rhs)
	{
		assert(m_handle == nullptr);
		std::swap(m_handle, rhs.m_handle);
	}

	struct gxm_texture *m_handle = nullptr;
};
