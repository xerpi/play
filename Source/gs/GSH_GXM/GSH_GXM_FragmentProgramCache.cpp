#include <assert.h>
#include "GSH_GXM.h"

SceGxmFragmentProgram *CGSH_GXM::FragmentProgramCacheGetOrInsert(FragmentProgramCache &cache, const SceGxmBlendInfo &blend_info, bool color_program)
{
	SceGxmFragmentProgram *fragment_program;
	union {
		SceGxmBlendInfo blend_info;
		uint32_t blend_info_u32;
	} key;

	key.blend_info = blend_info;

	auto entry = cache.find(key.blend_info_u32);
	if (entry != cache.end()) {
		fragment_program = entry->second;
	} else {
		if (color_program)
			gxm_create_color_fragment_program(&blend_info, &fragment_program);
		else
			gxm_create_texture_fragment_program(&blend_info, &fragment_program);

		cache[key.blend_info_u32] = fragment_program;
	}

	assert(fragment_program);

	return fragment_program;
}

void CGSH_GXM::FragmentProgramCacheClear(FragmentProgramCache &cache)
{
	for (auto &it: cache)
		gxm_release_fragment_program(it.second);

	cache.clear();
}
