#pragma once

#include <unordered_map>

#include "../GSHandler.h"
#include "../GsTextureCache.h"
#include "gxm.h"
#include "GxmTextureResource.h"

class CGSH_GXM : public CGSHandler
{
public:
	CGSH_GXM();
	~CGSH_GXM();

	void ProcessHostToLocalTransfer() override;
	void ProcessLocalToHostTransfer() override;
	void ProcessLocalToLocalTransfer() override;
	void ProcessClutTransfer(uint32, uint32) override;

	static FactoryFunction GetFactoryFunction();

protected:
	void WriteRegisterImpl(uint8, uint64);
	void InitializeImpl() override;
	void ReleaseImpl() override;
	void MarkNewFrame() override;
	void FlipImpl(const DISPLAY_INFO&) override;

private:
	struct VERTEX {
		uint64 position;
		uint64 rgbaq;
		uint64 uv;
		uint64 st;
		uint8 fog;
	};

	typedef void (CGSH_GXM::*TEXTUREUPDATER)(GxmTextureResource &, uint32, uint32, unsigned int, unsigned int, unsigned int, unsigned int);
	typedef std::unordered_map<uint32_t, SceGxmFragmentProgram *> FragmentProgramCache;

	void VertexKick(uint8, uint64);
	void SetRenderingContext(uint64);
	void SetupBlendingFunction(uint64 alphaReg);
	void SetupTestFunctions(uint64, uint64);
	void SetupDepthBuffer(uint64, uint64);
	void SetupTexture(uint64, uint64, uint64, uint64, uint64);
	struct gxm_texture *LoadTexture(const TEX0& tex0, uint32 maxMip, const MIPTBP1& miptbp1, const MIPTBP2& miptbp2);
	struct gxm_palette *GetClutTexture(const TEX0&);

	static SceGxmTextureFormat ToGxmTextureFormat(uint32 psm);
	static SceGxmDepthFunc ToGxmDepthFunc(uint32 depthMethod);
	static SceGxmTextureFilter ToGxmMinFilter(uint32 minFilter);
	static SceGxmTextureFilter ToGxmMagFilter(uint32 magFilter);
	static SceGxmBlendInfo ToGxmBlendInfo(const ALPHA &alpha);

	float CalcZ(float);
	void ToGxmVertexPosition(vector2f *position, const VERTEX *vin);
	void ToGxmVertexPosition(vector3f *position, const VERTEX *vin);
	void ToGxmVertexColor(uint32_t *color, const VERTEX *vin);
	void ToGxmVertexTexcoord(vector2f *texcoord, const VERTEX *vin);
	void ToGxmColorVertex(struct color_vertex *vout, const VERTEX *vin);
	void ToGxmTextureVertex(struct texture_vertex *vout, const VERTEX *vin);
	void Prim_Point();
	void Prim_Line();
	void Prim_Triangle();
	void Prim_Sprite();

	void SetupTextureUpdaters();
	void TexUpdater_Invalid(GxmTextureResource &, uint32, uint32, unsigned int, unsigned int, unsigned int, unsigned int);
	void TexUpdater_Psm32(GxmTextureResource &, uint32, uint32, unsigned int, unsigned int, unsigned int, unsigned int);
	template <typename> void TexUpdater_Psm16(GxmTextureResource &, uint32, uint32, unsigned int, unsigned int, unsigned int, unsigned int);
	template <typename> void TexUpdater_Psm48(GxmTextureResource &, uint32, uint32, unsigned int, unsigned int, unsigned int, unsigned int);
	template <uint32, uint32> void TexUpdater_Psm48H(GxmTextureResource &, uint32, uint32, unsigned int, unsigned int, unsigned int, unsigned int);

	static SceGxmFragmentProgram *FragmentProgramCacheGetOrInsert(FragmentProgramCache &cache, const SceGxmBlendInfo &blend_info, bool color_program);
	static void FragmentProgramCacheClear(FragmentProgramCache &cache);

	// Draw context
	VERTEX m_vtxBuffer[3];
	uint32 m_vtxCount = 0;
	uint32 m_primitiveType = 0;
	PRMODE m_primitiveMode;
	uint32 m_currentTextureWidth = 0;
	uint32 m_currentTextureHeight = 0;
	float m_nMaxZ = 0;
	float m_primOfsX = 0;
	float m_primOfsY = 0;
	uint32 m_texWidth = 0;
	uint32 m_texHeight = 0;

	SceGxmFragmentProgram *alpha_blending_off_color_fragment_program;
	SceGxmFragmentProgram *alpha_blending_off_texture_fragment_program;
	FragmentProgramCache m_fragmentProgramCacheColor;
	FragmentProgramCache m_fragmentProgramCacheTexture;

	TEXTUREUPDATER m_textureUpdater[CGSHandler::PSM_MAX];

	CGsTextureCache<GxmTextureResource> m_textureCache;
	struct gxm_palette m_clutTexture4;
	struct gxm_palette m_clutTexture8;
};
