#include <cstring>
#include "GSH_GXM.h"
#include "../../gs/GsPixelFormats.h"
#include "../../gs/GsTransferRange.h"
#include "../../Log.h"
#include "gxm.h"

#include <psp2/kernel/threadmgr.h>

#define LOG(...) printf(__VA_ARGS__)

static inline uint32_t rgba_u32_color(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	return (a << 24) | (b << 16) | (g << 8) | (r);
}

static inline uint8_t mul2_clamp(uint8_t value)
{
	if (value >= 0x80)
		return 0xFF;
	return value << 1;
}

CGSH_GXM::CGSH_GXM()
{
}

CGSH_GXM::~CGSH_GXM()
{
}

void CGSH_GXM::InitializeImpl()
{
	LOG("CGSH_GXM start\n");

	gxm_init();

	PRESENTATION_PARAMS presentationParams;
	presentationParams.mode = CGSHandler::PRESENTATION_MODE::PRESENTATION_MODE_ORIGINAL;
	presentationParams.windowWidth = 960;
	presentationParams.windowHeight = 544;

	SetPresentationParams(presentationParams);
	SetupTextureUpdaters();

	gxm_create_palette(&m_clutTexture4, SCE_GXM_TEXTURE_BASE_FORMAT_P8); // TODO: Use P4
	gxm_create_palette(&m_clutTexture8, SCE_GXM_TEXTURE_BASE_FORMAT_P8);

	static const SceGxmBlendInfo alpha_blending_off_blend_info = {
		.colorMask = SCE_GXM_COLOR_MASK_ALL,
		.colorFunc = SCE_GXM_BLEND_FUNC_NONE,
		.alphaFunc = SCE_GXM_BLEND_FUNC_NONE,
		.colorSrc  = SCE_GXM_BLEND_FACTOR_ONE,
		.colorDst  = SCE_GXM_BLEND_FACTOR_ONE,
		.alphaSrc  = SCE_GXM_BLEND_FACTOR_ONE,
		.alphaDst  = SCE_GXM_BLEND_FACTOR_ONE,
	};

	gxm_create_color_fragment_program(&alpha_blending_off_blend_info,
		&alpha_blending_off_color_fragment_program);

	gxm_create_texture_fragment_program(&alpha_blending_off_blend_info,
		&alpha_blending_off_texture_fragment_program);

	gxm_begin_scene();
}

void CGSH_GXM::ReleaseImpl()
{
	gxm_end_scene();
	gxm_terminate();

	gxm_free_palette(&m_clutTexture4);
	gxm_free_palette(&m_clutTexture8);

	gxm_release_fragment_program(alpha_blending_off_color_fragment_program);
	gxm_release_fragment_program(alpha_blending_off_texture_fragment_program);

	FragmentProgramCacheClear(m_fragmentProgramCacheColor);
	FragmentProgramCacheClear(m_fragmentProgramCacheTexture);
}

void CGSH_GXM::ProcessHostToLocalTransfer()
{
	LOG("CGSH_GXM::ProcessHostToLocalTransfer\n");

	if (m_trxCtx.nDirty) {
		gxm_end_scene();
		gxm_finish();
		gxm_vertex_buffer_color_reset();
		gxm_vertex_buffer_texture_reset();
		gxm_begin_scene();

		auto bltBuf = make_convertible<BITBLTBUF>(m_nReg[GS_REG_BITBLTBUF]);
		auto trxReg = make_convertible<TRXREG>(m_nReg[GS_REG_TRXREG]);
		auto trxPos = make_convertible<TRXPOS>(m_nReg[GS_REG_TRXPOS]);

		auto [transferAddress, transferSize] = GsTransfer::GetDstRange(bltBuf, trxReg, trxPos);

		m_textureCache.InvalidateRange(transferAddress, transferSize);
	}
}

void CGSH_GXM::ProcessLocalToHostTransfer()
{
	LOG("CGSH_GXM::ProcessLocalToHostTransfer\n");
	assert(false);
}

void CGSH_GXM::ProcessLocalToLocalTransfer()
{
	LOG("CGSH_GXM::ProcessLocalToLocalTransfer\n");
	assert(false);
}

void CGSH_GXM::ProcessClutTransfer(uint32, uint32)
{
	LOG("CGSH_GXM::ProcessClutTransfer\n");
	assert(false);
}

SceGxmTextureFormat CGSH_GXM::ToGxmTextureFormat(uint32 psm)
{
	switch (psm) {
	default:
		assert(false);
		[[fallthrough]];
	case PSMCT32:
		return SCE_GXM_TEXTURE_FORMAT_U8U8U8U8_ABGR;
	case PSMCT24:
		return SCE_GXM_TEXTURE_FORMAT_X8U8U8U8_1BGR;
	case PSMCT16:
	case PSMCT16S:
		return SCE_GXM_TEXTURE_FORMAT_U1U5U5U5_ABGR;
	case PSMT8:
	case PSMT4:
	case PSMT8H:
	case PSMT4HL:
	case PSMT4HH:
		return SCE_GXM_TEXTURE_FORMAT_P8_ABGR;
	}
}

SceGxmDepthFunc CGSH_GXM::ToGxmDepthFunc(uint32 depthMethod)
{
	switch (depthMethod) {
	default:
		assert(false);
		[[fallthrough]];
	case DEPTH_TEST_NEVER:
		return SCE_GXM_DEPTH_FUNC_NEVER;
	case DEPTH_TEST_ALWAYS:
		return SCE_GXM_DEPTH_FUNC_ALWAYS;
	case DEPTH_TEST_GEQUAL:
		return SCE_GXM_DEPTH_FUNC_GREATER_EQUAL;
	case DEPTH_TEST_GREATER:
		return SCE_GXM_DEPTH_FUNC_GREATER;
	}
}

SceGxmTextureFilter CGSH_GXM::ToGxmMinFilter(uint32 minFilter)
{
	switch (minFilter) {
	default:
		assert(false);
		[[fallthrough]];
	case MIN_FILTER_NEAREST:
	case MIN_FILTER_NEAREST_MIP_NEAREST:
	case MIN_FILTER_NEAREST_MIP_LINEAR:
		return SCE_GXM_TEXTURE_FILTER_POINT;
	case MIN_FILTER_LINEAR:
	case MIN_FILTER_LINEAR_MIP_NEAREST:
	case MIN_FILTER_LINEAR_MIP_LINEAR:
		return SCE_GXM_TEXTURE_FILTER_LINEAR;
	}
}

SceGxmTextureFilter CGSH_GXM::ToGxmMagFilter(uint32 magFilter)
{
	switch (magFilter) {
	default:
		assert(false);
		[[fallthrough]];
	case MAG_FILTER_NEAREST:
		return SCE_GXM_TEXTURE_FILTER_POINT;
	case MAG_FILTER_LINEAR:
		return SCE_GXM_TEXTURE_FILTER_LINEAR;
	}
}

SceGxmBlendInfo CGSH_GXM::ToGxmBlendInfo(const ALPHA &alpha)
{
	SceGxmBlendInfo blend_info;
	SceGxmBlendFunc color_func = SCE_GXM_BLEND_FUNC_ADD;
	SceGxmBlendFactor color_src_factor = SCE_GXM_BLEND_FACTOR_ONE;
	SceGxmBlendFactor color_dst_factor = SCE_GXM_BLEND_FACTOR_ZERO;

	// Cd' = (A - B) * C + D

	if ((alpha.nA == alpha.nB) && (alpha.nD == ALPHABLEND_ABD_CS)) {
		//ab*0 (when a == b) - Cs
		color_src_factor = SCE_GXM_BLEND_FACTOR_ONE;
		color_dst_factor = SCE_GXM_BLEND_FACTOR_ZERO;
	} else if ((alpha.nA == alpha.nB) && (alpha.nD == ALPHABLEND_ABD_CD)) {
		//ab*1 (when a == b) - Cd
		color_src_factor = SCE_GXM_BLEND_FACTOR_ZERO;
		color_dst_factor = SCE_GXM_BLEND_FACTOR_ONE;
	} else if ((alpha.nA == alpha.nB) && (alpha.nD == ALPHABLEND_ABD_ZERO)) {
		//ab*2 (when a == b) - Zero
		color_src_factor = SCE_GXM_BLEND_FACTOR_ZERO;
		color_dst_factor = SCE_GXM_BLEND_FACTOR_ZERO;
	} else if ((alpha.nA == ALPHABLEND_ABD_CS) && (alpha.nB == ALPHABLEND_ABD_CD) && (alpha.nC == ALPHABLEND_C_AS) && (alpha.nD == ALPHABLEND_ABD_CD)) {
		//0101 - Cs * As + Cd * (1 - As)
		color_src_factor = SCE_GXM_BLEND_FACTOR_SRC_ALPHA;
		color_dst_factor = SCE_GXM_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
	} else if ((alpha.nA == ALPHABLEND_ABD_CS) && (alpha.nB == ALPHABLEND_ABD_CD) && (alpha.nC == ALPHABLEND_C_AD) && (alpha.nD == ALPHABLEND_ABD_CD)) {
		//0111 - Cs * Ad + Cd * (1 - Ad)
		color_src_factor = SCE_GXM_BLEND_FACTOR_DST_ALPHA;
		color_dst_factor = SCE_GXM_BLEND_FACTOR_ONE_MINUS_DST_ALPHA;
	} else if ((alpha.nA == ALPHABLEND_ABD_CS) && (alpha.nB == ALPHABLEND_ABD_CD) && (alpha.nC == ALPHABLEND_C_FIX) && (alpha.nD == ALPHABLEND_ABD_CD)) {
		//0121 - Cs * FIX + Cd * (1 - FIX)
		//uint8_t fix = mul2_clamp(alpha.nFix);
		//color_src_factor = SCE_GXM_BLEND_FACTOR_BLENDFACTOR;
		//color_dst_factor = SCE_GXM_BLEND_FACTOR_INVBLENDFACTOR;
		//m_device->SetRenderState(D3DRS_BLENDFACTOR, D3DCOLOR_ARGB(fix, fix, fix, fix);
		assert(false);
	} else if ((alpha.nA == ALPHABLEND_ABD_CS) && (alpha.nB == ALPHABLEND_ABD_ZERO) && (alpha.nC == ALPHABLEND_C_FIX) && (alpha.nD == ALPHABLEND_ABD_CD)) {
		//0221 - Cs * FIX + Cd
		//uint8_t fix = mul2_clamp(alpha.nFix);
		//color_src_factor = SCE_GXM_BLEND_FACTOR_BLENDFACTOR;
		//color_dst_factor = SCE_GXM_BLEND_FACTOR_ONE;
		//m_device->SetRenderState(D3DRS_BLENDFACTOR, D3DCOLOR_ARGB(fix, fix, fix, fix);
		assert(false);
	} else if ((alpha.nA == ALPHABLEND_ABD_CS) && (alpha.nB == ALPHABLEND_ABD_ZERO) && (alpha.nC == ALPHABLEND_C_AS) && (alpha.nD == ALPHABLEND_ABD_CD)) {
		//0201
		color_src_factor = SCE_GXM_BLEND_FACTOR_SRC_ALPHA;
		color_dst_factor = SCE_GXM_BLEND_FACTOR_ONE;
	} else if ((alpha.nA == ALPHABLEND_ABD_CS) && (alpha.nB == ALPHABLEND_ABD_ZERO) && (alpha.nC == ALPHABLEND_C_AS) && (alpha.nD == ALPHABLEND_ABD_ZERO)) {
		//0202 - Cs * As
		color_src_factor = SCE_GXM_BLEND_FACTOR_SRC_ALPHA;
		color_dst_factor = SCE_GXM_BLEND_FACTOR_ZERO;
	} else if ((alpha.nA == ALPHABLEND_ABD_CS) && (alpha.nB == ALPHABLEND_ABD_ZERO) && (alpha.nC == ALPHABLEND_C_AD) && (alpha.nD == ALPHABLEND_ABD_CD)) {
		//0211 - Cs * Ad + Cd
		color_src_factor = SCE_GXM_BLEND_FACTOR_DST_ALPHA;
		color_dst_factor = SCE_GXM_BLEND_FACTOR_ONE;
	} else if ((alpha.nA == ALPHABLEND_ABD_CS) && (alpha.nB == ALPHABLEND_ABD_ZERO) && (alpha.nC == ALPHABLEND_C_FIX) && (alpha.nD == ALPHABLEND_ABD_CD)) {
		//0221
		assert(false);
	} else if ((alpha.nA == ALPHABLEND_ABD_CS) && (alpha.nB == ALPHABLEND_ABD_ZERO) && (alpha.nC == ALPHABLEND_C_FIX) && (alpha.nD == ALPHABLEND_ABD_ZERO)) {
		//0222 - Cs * FIX
		//glBlendColor(0, 0, 0, static_cast<float>(alpha.nFix) / 128.0f);
		//glBlendFuncSeparate(GL_CONSTANT_ALPHA, GL_ZERO, GL_ONE, GL_ZERO);
		assert(false);
	} else if ((alpha.nA == ALPHABLEND_ABD_CD) && (alpha.nB == ALPHABLEND_ABD_CS) && (alpha.nC == ALPHABLEND_C_AS) && (alpha.nD == ALPHABLEND_ABD_CS)) {
		//1000 -> (Cd - Cs) * As + Cs
		color_src_factor = SCE_GXM_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
		color_dst_factor = SCE_GXM_BLEND_FACTOR_SRC_ALPHA;
	} else if ((alpha.nA == ALPHABLEND_ABD_CD) && (alpha.nB == ALPHABLEND_ABD_CS) && (alpha.nC == ALPHABLEND_C_AS) && (alpha.nD == ALPHABLEND_ABD_CD)) {
		//1001 -> (Cd - Cs) * As + Cd (Inaccurate, needs +1 to As)
		color_src_factor = SCE_GXM_BLEND_FACTOR_SRC_ALPHA;
		color_dst_factor = SCE_GXM_BLEND_FACTOR_SRC_ALPHA;
		color_func = SCE_GXM_BLEND_FUNC_REVERSE_SUBTRACT;
	} else if ((alpha.nA == ALPHABLEND_ABD_CD) && (alpha.nB == ALPHABLEND_ABD_CS) && (alpha.nC == ALPHABLEND_C_AS) && (alpha.nD == ALPHABLEND_ABD_ZERO)) {
		//1002 -> (Cd - Cs) * As
		color_src_factor = SCE_GXM_BLEND_FACTOR_SRC_ALPHA;
		color_dst_factor = SCE_GXM_BLEND_FACTOR_SRC_ALPHA;
		color_func = SCE_GXM_BLEND_FUNC_REVERSE_SUBTRACT;
	} else if ((alpha.nA == ALPHABLEND_ABD_CD) && (alpha.nB == ALPHABLEND_ABD_CS) && (alpha.nC == ALPHABLEND_C_AD) && (alpha.nD == ALPHABLEND_ABD_CS)) {
		//1010 -> Cs * (1 - Ad) + Cd * Ad
		color_src_factor = SCE_GXM_BLEND_FACTOR_ONE_MINUS_DST_ALPHA;
		color_dst_factor = SCE_GXM_BLEND_FACTOR_DST_ALPHA;
	} else if ((alpha.nA == ALPHABLEND_ABD_CD) && (alpha.nB == ALPHABLEND_ABD_CS) && (alpha.nC == ALPHABLEND_C_FIX) && (alpha.nD == ALPHABLEND_ABD_CS)) {
		//1020 -> Cs * (1 - FIX) + Cd * FIX
		//glBlendColor(0, 0, 0, static_cast<float>(alpha.nFix) / 128.0f);
		//glBlendFuncSeparate(GL_ONE_MINUS_CONSTANT_ALPHA, GL_CONSTANT_ALPHA, GL_ONE, GL_ZERO);
		assert(false);
	} else if ((alpha.nA == ALPHABLEND_ABD_CD) && (alpha.nB == ALPHABLEND_ABD_CS) && (alpha.nC == ALPHABLEND_C_FIX) && (alpha.nD == ALPHABLEND_ABD_CD)) {
		//1021 -> (Cd - Cs) * FIX + Cd
		//nFunction = GL_FUNC_REVERSE_SUBTRACT;
		//glBlendColor(0.0f, 0.0f, 0.0f, (float)alpha.nFix / 128.0f);
		//glBlendFuncSeparate(GL_CONSTANT_ALPHA, GL_ONE, GL_ONE, GL_ZERO);
		assert(false);
	} else if ((alpha.nA == ALPHABLEND_ABD_CD) && (alpha.nB == ALPHABLEND_ABD_CS) && (alpha.nC == ALPHABLEND_C_FIX) && (alpha.nD == ALPHABLEND_ABD_ZERO)) {
		//1022 -> (Cd - Cs) * FIX
		//nFunction = GL_FUNC_REVERSE_SUBTRACT;
		//glBlendColor(0.0f, 0.0f, 0.0f, (float)alpha.nFix / 128.0f);
		//glBlendFuncSeparate(GL_CONSTANT_ALPHA, GL_CONSTANT_ALPHA, GL_ONE, GL_ZERO);
		assert(false);
	} else if ((alpha.nA == ALPHABLEND_ABD_CD) && (alpha.nB == ALPHABLEND_ABD_ZERO) && (alpha.nC == ALPHABLEND_C_AS) && (alpha.nD == ALPHABLEND_ABD_CS)) {
		//1200 -> Cd * As + Cs
		color_src_factor = SCE_GXM_BLEND_FACTOR_ONE;
		color_dst_factor = SCE_GXM_BLEND_FACTOR_SRC_ALPHA;
	} else if ((alpha.nA == ALPHABLEND_ABD_CD) && (alpha.nB == ALPHABLEND_ABD_ZERO) && (alpha.nC == ALPHABLEND_C_AS) && (alpha.nD == ALPHABLEND_ABD_CD)) {
		//1201 -> Cd * (As + 1)
		//Relies on colorOutputWhite shader cap
		//glBlendFuncSeparate(GL_DST_COLOR, BLEND_SRC_ALPHA, GL_ONE, GL_ZERO);
		assert(false);
	} else if ((alpha.nA == ALPHABLEND_ABD_CD) && (alpha.nB == ALPHABLEND_ABD_ZERO) && (alpha.nC == ALPHABLEND_C_AS) && (alpha.nD == ALPHABLEND_ABD_ZERO)) {
		//1202 - Cd * As
		color_src_factor = SCE_GXM_BLEND_FACTOR_ZERO;
		color_dst_factor = SCE_GXM_BLEND_FACTOR_SRC_ALPHA;
	} else if ((alpha.nA == ALPHABLEND_ABD_CD) && (alpha.nB == ALPHABLEND_ABD_ZERO) && (alpha.nC == ALPHABLEND_C_AD) && (alpha.nD == ALPHABLEND_ABD_CS)) {
		//1210 - Cs + (Cd * Ad)
		color_src_factor = SCE_GXM_BLEND_FACTOR_ONE;
		color_dst_factor = SCE_GXM_BLEND_FACTOR_DST_ALPHA;
	} else if ((alpha.nA == ALPHABLEND_ABD_CD) && (alpha.nB == ALPHABLEND_ABD_ZERO) && (alpha.nC == ALPHABLEND_C_AD) && (alpha.nD == ALPHABLEND_ABD_ZERO)) {
		//1212 - Cd * Ad
		color_src_factor = SCE_GXM_BLEND_FACTOR_ZERO;
		color_dst_factor = SCE_GXM_BLEND_FACTOR_DST_ALPHA;
	} else if ((alpha.nA == ALPHABLEND_ABD_CD) && (alpha.nB == ALPHABLEND_ABD_ZERO) && (alpha.nC == ALPHABLEND_C_FIX) && (alpha.nD == ALPHABLEND_ABD_CS)) {
		//1220 -> Cd * FIX + Cs
		//uint8_t fix = mul2_clamp(alpha.nFix);
		//color_src_factor = SCE_GXM_BLEND_FACTOR_ONE;
		//color_dst_factor = SCE_GXM_BLEND_FACTOR_BLENDFACTOR;
		//m_device->SetRenderState(D3DRS_BLENDFACTOR, D3DCOLOR_ARGB(fix, fix, fix, fix);
		assert(false);
	} else if ((alpha.nA == ALPHABLEND_ABD_CD) && (alpha.nB == ALPHABLEND_ABD_ZERO) && (alpha.nC == ALPHABLEND_C_FIX) && (alpha.nD == ALPHABLEND_ABD_CD)) {
		//1221 -> Cd * (1 + FIX)
		//Relies on colorOutputWhite shader cap
		//glBlendColor(0, 0, 0, static_cast<float>(alpha.nFix) / 128.0f);
		//glBlendFuncSeparate(GL_DST_COLOR, GL_CONSTANT_ALPHA, GL_ONE, GL_ZERO);
		assert(false);
	} else if ((alpha.nA == ALPHABLEND_ABD_CD) && (alpha.nB == ALPHABLEND_ABD_ZERO) && (alpha.nC == ALPHABLEND_C_FIX) && (alpha.nD == ALPHABLEND_ABD_ZERO)) {
		//1222 -> Cd * FIX
		//glBlendColor(0, 0, 0, static_cast<float>(alpha.nFix) / 128.0f);
		//glBlendFuncSeparate(GL_ZERO, GL_CONSTANT_ALPHA, GL_ONE, GL_ZERO);
		assert(false);
	} else if ((alpha.nA == ALPHABLEND_ABD_ZERO) && (alpha.nB == ALPHABLEND_ABD_CS) && (alpha.nC == ALPHABLEND_C_AS) && (alpha.nD == ALPHABLEND_ABD_CS)) {
		//2000 -> Cs * (1 - As)
		color_src_factor = SCE_GXM_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
		color_dst_factor = SCE_GXM_BLEND_FACTOR_ZERO;
	} else if ((alpha.nA == ALPHABLEND_ABD_ZERO) && (alpha.nB == ALPHABLEND_ABD_CS) && (alpha.nC == ALPHABLEND_C_AS) && (alpha.nD == ALPHABLEND_ABD_CD)) {
		//2001 -> Cd - Cs * As
		color_src_factor = SCE_GXM_BLEND_FACTOR_SRC_ALPHA;
		color_dst_factor = SCE_GXM_BLEND_FACTOR_ONE;
		color_func = SCE_GXM_BLEND_FUNC_REVERSE_SUBTRACT;
	} else if ((alpha.nA == ALPHABLEND_ABD_ZERO) && (alpha.nB == ALPHABLEND_ABD_CS) && (alpha.nC == ALPHABLEND_C_AD) && (alpha.nD == ALPHABLEND_ABD_CD)) {
		//2011 -> Cd - Cs * Ad
		color_src_factor = SCE_GXM_BLEND_FACTOR_DST_ALPHA;
		color_dst_factor = SCE_GXM_BLEND_FACTOR_ONE;
		color_func = SCE_GXM_BLEND_FUNC_REVERSE_SUBTRACT;
	} else if ((alpha.nA == ALPHABLEND_ABD_ZERO) && (alpha.nB == ALPHABLEND_ABD_CS) && (alpha.nC == ALPHABLEND_C_FIX) && (alpha.nD == ALPHABLEND_ABD_CD)) {
		//2021 -> Cd - Cs * FIX
		//nFunction = GL_FUNC_REVERSE_SUBTRACT;
		//glBlendColor(0, 0, 0, static_cast<float>(alpha.nFix) / 128.0f);
		//glBlendFuncSeparate(GL_CONSTANT_ALPHA, GL_ONE, GL_ONE, GL_ZERO);
		assert(false);
	} else if ((alpha.nA == ALPHABLEND_ABD_ZERO) && (alpha.nB == ALPHABLEND_ABD_CD) && (alpha.nC == ALPHABLEND_C_AS) && (alpha.nD == ALPHABLEND_ABD_CD)) {
		//2101 -> Cd * (1 - As)
		color_src_factor = SCE_GXM_BLEND_FACTOR_ZERO;
		color_dst_factor = SCE_GXM_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
	} else if ((alpha.nA == ALPHABLEND_ABD_ZERO) && (alpha.nB == ALPHABLEND_ABD_CD) && (alpha.nC == ALPHABLEND_C_FIX) && (alpha.nD == ALPHABLEND_ABD_CD)) {
		//2121 -> Cd * (1 - FIX)
		//glBlendColor(0, 0, 0, static_cast<float>(alpha.nFix) / 128.0f);
		//glBlendFuncSeparate(GL_ZERO, GL_ONE_MINUS_CONSTANT_ALPHA, GL_ONE, GL_ZERO);
		assert(false);
	} else {
		LOG("Unsupported alpha blending mode: nA=%d, nB=%d, nC=%d, nD=%d\n",
		    alpha.nA, alpha.nB, alpha.nC, alpha.nD);
		assert(false);
	}

	blend_info.colorMask = SCE_GXM_COLOR_MASK_ALL;
	blend_info.colorFunc = color_func;
	blend_info.alphaFunc = SCE_GXM_BLEND_FUNC_NONE;
	blend_info.colorSrc = color_src_factor;
	blend_info.colorDst = color_dst_factor;
	blend_info.alphaSrc = SCE_GXM_BLEND_FACTOR_ONE;
	blend_info.alphaDst = SCE_GXM_BLEND_FACTOR_ZERO;

	return blend_info;
}

float CGSH_GXM::CalcZ(float nZ)
{
	if (nZ < 256.0f)
		return nZ / 32768.0f;
	else if (nZ > m_nMaxZ)
		return 1.0f;
	else
		return nZ / m_nMaxZ;
}

void CGSH_GXM::ToGxmVertexPosition(vector2f *position, const VERTEX *vin)
{
	auto xyz = make_convertible<XYZ>(vin->position);
	position->x = xyz.GetX() - m_primOfsX;
	position->y = xyz.GetY() - m_primOfsY;
}

void CGSH_GXM::ToGxmVertexPosition(vector3f *position, const VERTEX *vin)
{
	auto xyz = make_convertible<XYZ>(vin->position);
	position->x = xyz.GetX() - m_primOfsX;
	position->y = xyz.GetY() - m_primOfsY;
	position->z = CalcZ(xyz.nZ);
}

void CGSH_GXM::ToGxmVertexColor(uint32_t *color, const VERTEX *vin)
{
	auto rgbaq = make_convertible<RGBAQ>(vin->rgbaq);
	*color = rgba_u32_color(rgbaq.nR, rgbaq.nG, rgbaq.nB, rgbaq.nA);
}

void CGSH_GXM::ToGxmVertexTexcoord(vector2f *texcoord, const VERTEX *vin)
{
	if (m_primitiveMode.nUseUV) {
		auto uv = make_convertible<UV>(vin->uv);
		texcoord->u = uv.GetU() / static_cast<float>(m_currentTextureWidth);
		texcoord->v = uv.GetV() / static_cast<float>(m_currentTextureHeight);
	} else {
		auto st = make_convertible<ST>(vin->st);
		auto rgbaq = make_convertible<RGBAQ>(vin->rgbaq);
		texcoord->u = st.nS / rgbaq.nQ;
		texcoord->v = st.nT / rgbaq.nQ;
	}
}

void CGSH_GXM::ToGxmColorVertex(struct color_vertex *vout, const VERTEX *vin)
{
	ToGxmVertexPosition(&vout->position, vin);
	ToGxmVertexColor(&vout->color, vin);
}

void CGSH_GXM::ToGxmTextureVertex(struct texture_vertex *vout, const VERTEX *vin)
{
	ToGxmVertexPosition(&vout->position, vin);
	ToGxmVertexColor(&vout->color, vin);
	ToGxmVertexTexcoord(&vout->texcoord, vin);
}

void CGSH_GXM::Prim_Point()
{
	struct color_vertex vertex;
	ToGxmColorVertex(&vertex, &m_vtxBuffer[0]);
	gxm_set_polygon_mode(SCE_GXM_POLYGON_MODE_POINT);
	gxm_vertex_buffer_color_push(SCE_GXM_PRIMITIVE_POINTS, &vertex, 1);
}

void CGSH_GXM::Prim_Line()
{
	struct color_vertex vertices[2];
	ToGxmColorVertex(&vertices[0], &m_vtxBuffer[1]);
	ToGxmColorVertex(&vertices[1], &m_vtxBuffer[0]);
	gxm_set_polygon_mode(SCE_GXM_POLYGON_MODE_LINE);
	gxm_vertex_buffer_color_push(SCE_GXM_PRIMITIVE_LINES, vertices, ARRAY_SIZE(vertices));
}

void CGSH_GXM::Prim_Triangle()
{
	gxm_set_polygon_mode(SCE_GXM_POLYGON_MODE_TRIANGLE_FILL);

	if (m_primitiveMode.nTexture) {
		struct texture_vertex vertices[3];
		ToGxmTextureVertex(&vertices[0], &m_vtxBuffer[2]);
		ToGxmTextureVertex(&vertices[1], &m_vtxBuffer[1]);
		ToGxmTextureVertex(&vertices[2], &m_vtxBuffer[0]);

		if (m_primitiveMode.nShading == 0)
			vertices[0].color = vertices[1].color = vertices[2].color;

		gxm_vertex_buffer_texture_push(SCE_GXM_PRIMITIVE_TRIANGLES, vertices, ARRAY_SIZE(vertices));
	} else {
		struct color_vertex vertices[3];
		ToGxmColorVertex(&vertices[0], &m_vtxBuffer[2]);
		ToGxmColorVertex(&vertices[1], &m_vtxBuffer[1]);
		ToGxmColorVertex(&vertices[2], &m_vtxBuffer[0]);

		if (m_primitiveMode.nShading == 0)
			vertices[0].color = vertices[1].color = vertices[2].color;

		gxm_vertex_buffer_color_push(SCE_GXM_PRIMITIVE_TRIANGLES, vertices, ARRAY_SIZE(vertices));
	}
}

void CGSH_GXM::Prim_Sprite()
{
	vector2f p0, p1;
	ToGxmVertexPosition(&p0, &m_vtxBuffer[1]);
	ToGxmVertexPosition(&p1, &m_vtxBuffer[0]);
	float z = CalcZ(make_convertible<XYZ>(m_vtxBuffer[0].position).nZ);

	uint32_t color;
	ToGxmVertexColor(&color, &m_vtxBuffer[0]);

	if (m_primitiveMode.nTexture) {
		vector2f t0, t1;
		ToGxmVertexTexcoord(&t0, &m_vtxBuffer[1]);
		ToGxmVertexTexcoord(&t1, &m_vtxBuffer[0]);

		struct texture_vertex vertices[4] = {
			{ .position = {p0.x, p0.y, z}, .color = color, .texcoord = {t0.u, t0.v} },
			{ .position = {p1.x, p0.y, z}, .color = color, .texcoord = {t1.u, t0.v} },
			{ .position = {p0.x, p1.y, z}, .color = color, .texcoord = {t0.u, t1.v} },
			{ .position = {p1.x, p1.y, z}, .color = color, .texcoord = {t1.u, t1.v} }
		};

		gxm_vertex_buffer_texture_push(SCE_GXM_PRIMITIVE_TRIANGLE_STRIP, vertices, ARRAY_SIZE(vertices));
	} else {
		struct color_vertex vertices[4] = {
			{ .position = {p0.x, p0.y, z}, .color = color },
			{ .position = {p1.x, p0.y, z}, .color = color },
			{ .position = {p0.x, p1.y, z}, .color = color },
			{ .position = {p1.x, p1.y, z}, .color = color }
		};

		gxm_vertex_buffer_color_push(SCE_GXM_PRIMITIVE_TRIANGLE_STRIP, vertices, ARRAY_SIZE(vertices));
	}

	/*for (int i = 0; i < ARRAY_SIZE(vertices); i++) {
		LOG("VTX {%f, %f, %f, 0x%08X}\n",
			vertices[i].position.x, vertices[i].position.y, vertices[i].position.z,
			vertices[i].color);
	}*/

	gxm_set_polygon_mode(SCE_GXM_POLYGON_MODE_TRIANGLE_FILL);
}

void CGSH_GXM::VertexKick(uint8 registerId, uint64 data)
{
	if (m_vtxCount == 0)
		return;

	bool drawingKick = (registerId == GS_REG_XYZ2) || (registerId == GS_REG_XYZF2);
	bool fog = (registerId == GS_REG_XYZF2) || (registerId == GS_REG_XYZF3);

	if (fog) {
		m_vtxBuffer[m_vtxCount - 1].position = data & 0x00FFFFFFFFFFFFFFULL;
		m_vtxBuffer[m_vtxCount - 1].rgbaq = m_nReg[GS_REG_RGBAQ];
		m_vtxBuffer[m_vtxCount - 1].uv = m_nReg[GS_REG_UV];
		m_vtxBuffer[m_vtxCount - 1].st = m_nReg[GS_REG_ST];
		m_vtxBuffer[m_vtxCount - 1].fog = static_cast<uint8>(data >> 56);
	} else {
		m_vtxBuffer[m_vtxCount - 1].position = data;
		m_vtxBuffer[m_vtxCount - 1].rgbaq = m_nReg[GS_REG_RGBAQ];
		m_vtxBuffer[m_vtxCount - 1].uv = m_nReg[GS_REG_UV];
		m_vtxBuffer[m_vtxCount - 1].st = m_nReg[GS_REG_ST];
		m_vtxBuffer[m_vtxCount - 1].fog = static_cast<uint8>(m_nReg[GS_REG_FOG] >> 56);
	}

	m_vtxCount--;

	if (m_vtxCount == 0) {
		if (m_nReg[GS_REG_PRMODECONT] & 1)
			m_primitiveMode <<= m_nReg[GS_REG_PRIM];
		else
			m_primitiveMode <<= m_nReg[GS_REG_PRMODE];

		if (drawingKick)
			SetRenderingContext(m_primitiveMode);

		switch (m_primitiveType) {
		case PRIM_POINT:
			if (drawingKick) Prim_Point();
			m_vtxCount = 1;
			break;
		case PRIM_LINE:
			if (drawingKick) Prim_Line();
			m_vtxCount = 2;
			break;
		case PRIM_LINESTRIP:
			if (drawingKick) Prim_Line();
			memcpy(&m_vtxBuffer[1], &m_vtxBuffer[0], sizeof(VERTEX));
			m_vtxCount = 1;
			break;
		case PRIM_TRIANGLE:
			if (drawingKick) Prim_Triangle();
			m_vtxCount = 3;
			break;
		case PRIM_TRIANGLESTRIP:
			if (drawingKick) Prim_Triangle();
			memcpy(&m_vtxBuffer[2], &m_vtxBuffer[1], sizeof(VERTEX));
			memcpy(&m_vtxBuffer[1], &m_vtxBuffer[0], sizeof(VERTEX));
			m_vtxCount = 1;
			break;
		case PRIM_TRIANGLEFAN:
			if (drawingKick) Prim_Triangle();
			memcpy(&m_vtxBuffer[1], &m_vtxBuffer[0], sizeof(VERTEX));
			m_vtxCount = 1;
			break;
		case PRIM_SPRITE:
			if (drawingKick) Prim_Sprite();
			m_vtxCount = 2;
			break;
		}
	}
}

void CGSH_GXM::SetRenderingContext(uint64 primReg)
{
	auto prim = make_convertible<PRMODE>(primReg);
	unsigned int context = prim.nContext;
	auto offset = make_convertible<XYOFFSET>(m_nReg[GS_REG_XYOFFSET_1 + context]);
	auto frame = make_convertible<FRAME>(m_nReg[GS_REG_FRAME_1 + context]);
	auto zbuf = make_convertible<ZBUF>(m_nReg[GS_REG_ZBUF_1 + context]);
	auto tex0 = make_convertible<TEX0>(m_nReg[GS_REG_TEX0_1 + context]);
	auto tex1 = make_convertible<TEX1>(m_nReg[GS_REG_TEX1_1 + context]);
	auto miptbp1 = make_convertible<MIPTBP1>(m_nReg[GS_REG_MIPTBP1_1 + context]);
	auto miptbp2 = make_convertible<MIPTBP2>(m_nReg[GS_REG_MIPTBP2_1 + context]);
	auto clamp = make_convertible<CLAMP>(m_nReg[GS_REG_CLAMP_1 + context]);
	auto alpha = make_convertible<ALPHA>(m_nReg[GS_REG_ALPHA_1 + context]);
	auto scissor = make_convertible<SCISSOR>(m_nReg[GS_REG_SCISSOR_1 + context]);
	auto test = make_convertible<TEST>(m_nReg[GS_REG_TEST_1 + context]);
	auto texA = make_convertible<TEXA>(m_nReg[GS_REG_TEXA]);
	auto fogCol = make_convertible<FOGCOL>(m_nReg[GS_REG_FOGCOL]);

	SetupTestFunctions(test, zbuf);
	SetupDepthBuffer(zbuf, frame);

	if (prim.nTexture) {
		SceGxmFragmentProgram *fragment_program;
		if (prim.nAlpha) {
			fragment_program = FragmentProgramCacheGetOrInsert(m_fragmentProgramCacheTexture,
				ToGxmBlendInfo(alpha), false);
		} else {
			fragment_program = alpha_blending_off_texture_fragment_program;
		}

		gxm_set_texture_fragment_program(fragment_program);
		SetupTexture(tex0, tex1, miptbp1, miptbp2, clamp);
	} else {
		SceGxmFragmentProgram *fragment_program;
		if (prim.nAlpha) {
			fragment_program = FragmentProgramCacheGetOrInsert(m_fragmentProgramCacheColor,
				ToGxmBlendInfo(alpha), true);
		} else {
			fragment_program = alpha_blending_off_color_fragment_program;
		}
		gxm_set_color_fragment_program(fragment_program);
	}

	m_primOfsX = offset.GetX();
	m_primOfsY = offset.GetY();

	if (GetCrtIsInterlaced() && GetCrtIsFrameMode()) {
		if (m_nCSR & CSR_FIELD)
			m_primOfsY += 0.5;
	}

	gxm_color_surfaces_set_clip(scissor.scax0, scissor.scay0,
		scissor.scax1 - scissor.scax0 + 1,
		scissor.scay1 - scissor.scay0 + 1);

	m_texWidth = tex0.GetWidth();
	m_texHeight = tex0.GetHeight();
}

void CGSH_GXM::SetupTestFunctions(uint64 testReg, uint64 zbufReg)
{
	auto test = make_convertible<TEST>(testReg);
	auto zbuf = make_convertible<ZBUF>(zbufReg);

	bool depth_write_enabled = (zbuf.nMask ? false : true);
	if ((test.nAlphaEnabled == 1) &&
	    (test.nAlphaMethod == ALPHA_TEST_NEVER) &&
	    ((test.nAlphaFail == ALPHA_TEST_FAIL_FBONLY) || (test.nAlphaFail == ALPHA_TEST_FAIL_RGBONLY))) {
		depth_write_enabled = false;
	}

	gxm_set_depth_write_mode(depth_write_enabled ?
		SCE_GXM_DEPTH_WRITE_ENABLED :
		SCE_GXM_DEPTH_WRITE_DISABLED);

	if (test.nDepthEnabled) {
		SceGxmDepthFunc func = ToGxmDepthFunc(test.nDepthMethod);
		gxm_set_depth_test_function(func);
	}
}

void CGSH_GXM::SetupDepthBuffer(uint64 zbufReg, uint64 frameReg)
{
	auto frame = make_convertible<FRAME>(frameReg);
	auto zbuf = make_convertible<ZBUF>(zbufReg);

	switch (CGsPixelFormats::GetPsmPixelSize(zbuf.nPsm)) {
	case 16:
		m_nMaxZ = 32768.0f;
		break;
	case 24:
		m_nMaxZ = 8388608.0f;
		break;
	default:
	case 32:
		m_nMaxZ = 2147483647.0f;
		break;
	}
}

void CGSH_GXM::SetupTexture(uint64 tex0Reg, uint64 tex1Reg, uint64 miptbp1Reg, uint64 miptbp2Reg, uint64 clampReg)
{
	auto tex0 = make_convertible<TEX0>(tex0Reg);
	auto tex1 = make_convertible<TEX1>(tex1Reg);
	auto miptbp1 = make_convertible<MIPTBP1>(miptbp1Reg);
	auto miptbp2 = make_convertible<MIPTBP2>(miptbp2Reg);
	auto clamp = make_convertible<CLAMP>(clampReg);
	struct gxm_texture *texture = LoadTexture(tex0, tex1.nMaxMip, miptbp1, miptbp2);

	SceGxmTextureFilter min_filter = ToGxmMinFilter(tex1.nMinFilter);
	SceGxmTextureFilter mag_filter = ToGxmMagFilter(tex1.nMagFilter);
	SceGxmTextureMipFilter mip_filter = SCE_GXM_TEXTURE_MIP_FILTER_DISABLED;

	static const SceGxmTextureAddrMode clamp_modes[CGSHandler::CLAMP_MODE_MAX] = {
		[CLAMP_MODE_REPEAT]        = SCE_GXM_TEXTURE_ADDR_REPEAT,
		[CLAMP_MODE_CLAMP]         = SCE_GXM_TEXTURE_ADDR_CLAMP,
		[CLAMP_MODE_REGION_CLAMP]  = SCE_GXM_TEXTURE_ADDR_REPEAT,
		[CLAMP_MODE_REGION_REPEAT] = SCE_GXM_TEXTURE_ADDR_REPEAT
	};

	if (CGsPixelFormats::IsPsmIDTEX(tex0.nPsm)) {
		if (min_filter == SCE_GXM_TEXTURE_FILTER_LINEAR)
			min_filter = SCE_GXM_TEXTURE_FILTER_POINT;
		if (mag_filter == SCE_GXM_TEXTURE_FILTER_LINEAR)
			mag_filter = SCE_GXM_TEXTURE_FILTER_POINT;
		if (mip_filter == SCE_GXM_TEXTURE_MIP_FILTER_ENABLED)
			mip_filter = SCE_GXM_TEXTURE_MIP_FILTER_DISABLED;

		gxm_texture_set_palette(texture, GetClutTexture(tex0));
	}

	gxm_texture_set_min_filter(texture, min_filter);
	gxm_texture_set_mag_filter(texture, mag_filter);
	gxm_texture_set_mip_filter(texture, mip_filter);
	gxm_texture_set_u_addr_mode(texture, clamp_modes[clamp.nWMS]);
	gxm_texture_set_v_addr_mode(texture, clamp_modes[clamp.nWMT]);
	gxm_texture_set_fragment_texture(texture, 0);

	m_currentTextureWidth = tex0.GetWidth();
	m_currentTextureHeight = tex0.GetHeight();
}

void CGSH_GXM::WriteRegisterImpl(uint8 registerId, uint64 data)
{
	CGSHandler::WriteRegisterImpl(registerId, data);

	switch (registerId) {
	case GS_REG_PRIM:
		m_primitiveType = static_cast<unsigned int>(data & 0x07);
		switch (m_primitiveType) {
		case PRIM_POINT:
			m_vtxCount = 1;
			break;
		case PRIM_LINE:
		case PRIM_LINESTRIP:
			m_vtxCount = 2;
			break;
		case PRIM_TRIANGLE:
		case PRIM_TRIANGLESTRIP:
		case PRIM_TRIANGLEFAN:
			m_vtxCount = 3;
			break;
		case PRIM_SPRITE:
			m_vtxCount = 2;
			break;
		}
		break;
	case GS_REG_XYZ2:
	case GS_REG_XYZ3:
	case GS_REG_XYZF2:
	case GS_REG_XYZF3:
		VertexKick(registerId, data);
		break;
	}
}

void CGSH_GXM::MarkNewFrame()
{
	LOG("CGSH_GXM::MarkNewFrame\n");

	gxm_end_scene();
	gxm_finish();
	gxm_queue_flip();
	gxm_vertex_buffer_color_reset();
	gxm_vertex_buffer_texture_reset();
	gxm_begin_scene();

	CGSHandler::MarkNewFrame();
}

void CGSH_GXM::FlipImpl(const DISPLAY_INFO& dispInfo)
{
	LOG("CGSH_GXM::FlipImpl\n");

	CGSHandler::FlipImpl(dispInfo);
}

CGSHandler::FactoryFunction CGSH_GXM::GetFactoryFunction()
{
	return [=]() { return new CGSH_GXM(); };
}
